(function() {
    'use strict';

    angular.module('modal.agenda.novoevento').controller('ModalAgendaNovoEventoController', ModalAgendaNovoEventoController);

	ModalAgendaNovoEventoController.$inject = ['$scope', 'DateUtilService', '$modalInstance', 'dataInicio', 'dataFim', 'cliente'];

    function ModalAgendaNovoEventoController($scope, DateUtilService, $modalInstance, dataInicio, dataFim, cliente) {
        
		var vm = this;

		// Objeto que irá controlar a visibilidade dos itens no modal.
		vm.controlePagina = {
	
			isVisivelBuscarPessoa : false,       //Exibir a opção de buscar pessoa.
			isVisivelFormPessoa : false,         //Exibir a opção do form de cadastrar pessoa.
			isVisivelPessoaSelecionada: false,   //Exibir as informações da pessoa selecionada na busca.
			isVisivelBotaoEditarPessoa :false,   //Exibir o botão de editar pessoa
			isVisivelBotaoIncluirPessoa : false, //Exibir o botão de incluir pessoa

			exibirIncluirPessoa:function () {
				vm.pessoaSelecionada = [];
				this.isVisivelPessoaSelecionada = false;
				this.isVisivelBuscarPessoa = false;
				this.isVisivelFormPessoa = true;
				this.isVisivelBotaoIncluirPessoa = true;
				this.isVisivelBotaoEditarPessoa = false;
			},
			
			exibirBuscarPessoa:function() {
				vm.pessoaSelecionada = [];
				this.isVisivelFormPessoa = false;
				this.isVisivelBuscarPessoa = true;
				this.isVisivelPessoaSelecionada = false;
				this.isVisivelBotaoIncluirPessoa = false;
				this.isVisivelBotaoEditarPessoa = false;
			},
			
			exibirPessoaSelecionada:function() {
				this.isVisivelFormPessoa = false;
				this.isVisivelBuscarPessoa = false;
				this.isVisivelPessoaSelecionada = true;
				this.isVisivelPessoaSelecionadaBotoes = true;
				this.isVisivelBotaoIncluirPessoa = false;
				this.isVisivelBotaoEditarPessoa = false;
			},
			
			exibirPessoaUsuarioFixo: function() {
				this.isVisivelFormPessoa = false;
				this.isVisivelBuscarPessoa = false;
				this.isVisivelPessoaSelecionada = true;
				this.isVisivelPessoaSelecionadaBotoes = false;
				this.isVisivelBotaoIncluirPessoa = false;
				this.isVisivelBotaoEditarPessoa = false;
			},
			
			exibirEditarPessoa:function () {
				this.isVisivelPessoaSelecionada = false;
				this.isVisivelBuscarPessoa = false;
				this.isVisivelFormPessoa = true;
				this.isVisivelBotaoIncluirPessoa = false;
				this.isVisivelBotaoEditarPessoa = true;
			}

		};
		
		//**************************************  BASICO  ******************************************
		
		//Usado para definir a menor data que o usuário pode selecionar.
		vm.dataMinima = moment().subtract(1, 'days');
		
		//Criando uma cópia das duas datas enviadas por parâmetro do componente Modal.
		vm.dataInicioSelecionada = angular.copy(dataInicio);
		vm.dataFimSelecionada    = angular.copy(dataFim);

		//Definindo data inicio e fim em formato javascript básico para serem usados em componentes que não possuem suporte ao wrapper moment.
		vm.dataInicio =   DateUtilService.getJavaScriptDateFromMoment(vm.dataInicioSelecionada);
		vm.dataFim    =   DateUtilService.getJavaScriptDateFromMoment(vm.dataFimSelecionada);
		
		//***********************************  BUSCA PESSOA  ***************************************

		vm.pessoaSelecionada = {}; // Pessoa selecionada na busca
		vm.pessoaNova = {};        // Pessoa a ser cadastrada/editada.
		vm.pacientes = [];         // Listagem de pessoas.

		//**************************************  DATAS  *******************************************
		
		$scope.format = 'dd-MMMM-yyyy';
  
		//Objeto usado para definir quais pop up date que está aberta.
		$scope.open = {
			dataCompleta:false,
			dataInicio: false,
			dataFim: false
		};
		
		//Define as opções dos objetos datetime.
		$scope.timeOptions = {
			readonlyInput: true,
			showMeridian: false
		};
		
		$scope.dateOptions = {
			startingDay: 1
		};

		//*********************************  METODOS PUBLICOS  *************************************

		vm.openCalendar = openCalendar;
		
		//*********************************  METODOS PRIVADOS  *************************************

		//****  date/datetime  ***
		

		//Função usada para abrir o pop up do calendario ou datetime.
		function openCalendar(e, date) {
			
			e.preventDefault();
			e.stopPropagation();
			
			if(date == 'dataInicio'){
				
				$scope.open.dataFim = false;
				$scope.open.dataCompleta = false;
				
			}else if(date == 'dataFim'){
				
				$scope.open.dataInicio = false;
				$scope.open.dataCompleta = false;
				
			}else{
				$scope.open.dataInicio = false;
				$scope.open.dataFim = false;
			}

			$scope.open[date] = true;

		};

		//Função chamada quando uma pessoa for selecionada na busca.
		$scope.pessoaSelected = function(selected) {
			vm.pessoaSelecionada = selected.originalObject;
			vm.controlePagina.exibirPessoaSelecionada();
		};
		
		//Função chamada quando o usuário deseja editar informações do paciente selecionado.
		$scope.exibirEditar = function () {
			vm.pessoaNova = getObjectList(vm.pessoaSelecionada.id,"id",vm.pacientes);
			vm.controlePagina.exibirEditarPessoa();
		};
		
		//Função responsavel por salvar os dados editados do paciente.
		$scope.editarPessoa = function () {
			vm.pacientes = removeObjectList(vm.pessoaNova.id,"id",vm.pacientes);
			vm.pacientes.push(vm.pessoaNova);
			vm.pessoaSelecionada = vm.pessoaNova;
			vm.controlePagina.exibirPessoaSelecionada();
		};
		
		//Função responsavel por salvar os dados editados do paciente.
		$scope.incluirPessoa = function () {
			vm.pessoaNova.pic = "img/semfoto.jpg";
			vm.pacientes.push(vm.pessoaNova);
			vm.pessoaSelecionada = vm.pessoaNova;
			vm.controlePagina.exibirPessoaSelecionada();
		};
	
		//Função respons�vel por inserir um novo evento no calendario.
		$scope.inserir = function () {
			
			var novoEvento = {};
			novoEvento.id = 10;
			novoEvento.title = vm.nomeEvento + " - " + vm.nomePaciente;
			novoEvento.start = moment(vm.dataInicio).format();
			//novoEvento.start = "2015-09-07T09:50:00";
			//alert(vm.dataInicio);
			//alert
			
			novoEvento.end = moment(vm.dataFim);
			novoEvento.textColor = "white";
			novoEvento.backgroundColor = "pink";
			
			//Retornando o novo objeto criado.
			$modalInstance.close(novoEvento);
		};
		
		// Caso o usuario cancele a operacao.
		$scope.cancelar = function () {
			$modalInstance.close();
		};

		//Função Genericas ( A serem colocadas em um servi�o );
		function getObjectList(indice,prop,lista){
			for(var i = 0 ; i < lista.length ; i++){
				if(lista[i][prop] == indice){
					return angular.copy(lista[i]);
				}
			}
		};
		function removeObjectList(indice,prop,lista) {
			alert(indice);
			for(var i = 0; i < lista.length; i++){
				if(lista[i][prop] == indice){
					 lista.splice(i,1);
					 break;
				}
			}
			return angular.copy(lista);
		};

		//Elementos FAKE;
		$scope.getListaPessoa = function(){
		
			vm.pacientes = [
			
			  { id:"10", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Victor", surname: "Sarrasqueiro", rg: "020-722-138-3", pic: "img/victor.jpg"},
			  { id:"11", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Carlos", surname: "Lopes", rg: "780-742-158-4", pic: "img/carlos.jpg"},
			  { id:"12", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Axl", surname: "Rose", rg: "040-526-188-7", pic: "img/axl.png"},
			  { id:"13", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Cassia", surname: "Eller", rg: "577-462-040-4", pic: "img/cassia.jpg"},
			  { id:"14", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Cazuza", surname: "", rg: "950-122-080-8", pic: "img/cazuza.jpg"},
			  { id:"15", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Edge", surname: "", rg: "999-754-844-1", pic: "img/edge.jpg"},
			  { id:"16", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Michael", surname: "Jackson", rg: "878-708-184-1", pic: "img/michael.jpg"},
			  { id:"17", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Renato", surname: "Russo", rg: "455-118-909-0", pic: "img/renato.jpg"},
			  { id:"18", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Saint", surname: "Seya", rg: "146-877-171-8", pic: "img/seya.jpg"},
			  { id:"19", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Tom", surname: "Hanks", rg: "786-844-178-4", pic: "img/tom.jpg"},
			  { id:"100", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "James", surname: "Camareon", rg: "787-400-111-3", pic: "img/james.jpg"},
			  { id:"101", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Michael", surname: "Jackson", rg: "878-708-184-1", pic: "img/michael.jpg"},
			  { id:"102", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Bono", surname: "Vox", rg: "250-262-274-1", pic: "img/bono.png"}
			];

		};
		
		//Elementos FAKE;
		$scope.getUsuarioPorId = function(id){

			return { id:"10", email:"teste@gmail.com", convenio:"1", telefone: "2133910817", celular:"21992992720", whatsapp:"21992992720", firstName: "Victor", surname: "Sarrasqueiro", rg: "020-722-138-3", pic: "img/victor.jpg"};

		}
		
		//**************************************  INICIO  ******************************************
		
		
		
		//****  Inicio do processo  ****//
		function activate() {
			
			if(cliente == null || cliente.id == 0){
			
				//Recuperando a lista de pessoas para ser usada na busca.
				$scope.getListaPessoa();		
			
			}else{
			
				// Recuperando o usuario selecionado.
				vm.pessoaSelecionada = $scope.getUsuarioPorId(cliente);
				vm.controlePagina.exibirPessoaUsuarioFixo();
			
			}

        };
		
		
		//Invocando a função inicio.
		activate();

    }

})();

