/**
 * Created by gabriel.lima on 06/05/2015.
 */

(function() {

    'use strict';

    angular.module('app.atividade').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [{
            state: 'app.novaAtividade',
            config: {
                url: '/atividade/nova',
                title: 'Nova Atividade',
                views: {
                    'content@app': {
                        templateUrl: 'app/atividade/nova-atividade.html',
                        controller: 'AtividadeController',
                        controllerAs: 'vm'
                    }
                },
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        },{
            state: 'app.novaAtividadeIndependente',
            config: {
                url: '/atividade/independente/nova',
                title: 'Nova Atividade Independente',
                views: {
                    'content@app': {
                        templateUrl: 'app/atividade/nova-atividade-independente.html',
                        controller: 'AtividadeController',
                        controllerAs: 'vm'
                    }
                },
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        },{
            state: 'app.novaAtividadeIncidente',
            config: {
                url: '/atividade/incidente/nova',
                title: 'Nova Atividade Incidente',
                views: {
                    'content@app': {
                        templateUrl: 'app/atividade/nova-atividade-incidente.html',
                        controller: 'AtividadeController',
                        controllerAs: 'vm'
                    }
                },
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        },{
            state: 'app.novaAtividadeDemanda',
            config: {
                url: '/atividade/demanda/nova',
                title: 'Nova Atividade Demanda',
                views: {
                    'content@app': {
                        templateUrl: 'app/atividade/nova-atividade-demanda.html',
                        controller: 'AtividadeController',
                        controllerAs: 'vm'
                    }
                },
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        },{
            state: 'app.atividade',
            config: {
                url: '/atividade',
                title: 'Lista de Atividades',
                views: {
                    'content@app': {
                        templateUrl: 'app/atividade/atividade.html',
                        controller: 'AtividadeController',
                        controllerAs: 'vm'
                    }
                },
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        }];
    }

})();
