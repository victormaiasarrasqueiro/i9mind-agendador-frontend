(function() {
    'use strict';

    angular.module('app.atividade').controller('AtividadeController', AtividadeController);

	AtividadeController.$inject = ['$log', '$http', '$scope', '$modal'];

    function AtividadeController( $log, $http, $scope, $modal) {
        
		var vm = this; // vm = view-model

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		var dbEventos = [
			
			{
				id     : 1,
				title  : 'Evento Banco de Dados Teste 1',
				start  : '2015-08-04T13:30:00',
				end    : '2015-08-04T15:30:00',
				backgroundColor  : "yellow",
				borderColor  : "black", //  Borda -> IMPORTANTE
				textColor: "black",
				overlap: false   // ( SOBREPOSICAO = FALSE ) N�O � Possivel combinar este evento na mesma hora de outro evento.
			},
			{
				id     : 2,
				title  : 'Evento Banco de Dados Teste 2',
				start  : '2015-08-03T12:00:00',
				end    : '2015-08-03T12:30:00',
				backgroundColor  : "blue",
				textColor: "white",
				constraint: "businessHours" // N�o � possivel mover o objeto para uma area fora da business Hours
			},
			{
				id     : 3,
				title  : 'Evento Banco de Dados Teste 3',
				start  : '2015-08-04T09:00:00',
				end    : '2015-08-04T11:30:00',
				backgroundColor  : "#EF820D",
				textColor: "blue",
				borderColor  : "black" //  Borda -> IMPORTANTE 
			},
			{
				id     : 4,
				title  : 'Evento Banco de Dados Teste 4',
				start  : '2015-08-04T17:00:00',
				end    : '2015-08-04T18:00:00',
				textColor: "white",
				backgroundColor  : "green"
			},
			{
				id     : 5,
				title  : 'Evento Google Teste 1',
				start  : '2015-08-05T09:30:00',
				end    : '2015-08-05T09:50:00',
				backgroundColor  : "yellow",
				borderColor  : "black", //  Borda -> IMPORTANTE
				textColor: "black",
				overlap: false   // ( SOBREPOSICAO = FALSE ) N�O � Possivel combinar este evento na mesma hora de outro evento.
			},
			{
				id     : 6,
				title  : 'Evento Google Teste 2',
				start  : '2015-08-06T10:30:00',
				end    : '2015-08-06T11:00:00',
				backgroundColor  : "blue",
				textColor: "white",
				constraint: "businessHours" // N�o � possivel mover o objeto para uma area fora da business Hours
			},
			{
				id     : 7,
				title  : 'Evento Google Teste 3',
				start  : '2015-08-05T13:30:00',
				end    : '2015-08-05T14:00:00',
				backgroundColor  : "#EF820D",
				textColor: "blue",
				borderColor  : "black" //  Borda -> IMPORTANTE 
			},
			{
				id     : 8,
				title  : 'Evento Google Teste 4',
				start  : '2015-08-05T14:15:00',
				end    : '2015-08-05T15:00:00',
				textColor: "white",
				backgroundColor  : "green"
			},
			{
				id     : "#",
				start: '2015-08-06T16:15:00',
				end:   '2015-08-06T18:00:00',
				overlap: false,
				selectable:false,
				rendering: 'background',
				color: '#ff9f89'
			}
		];

		vm.eventSources = [
			dbEventos
		];
		
		//$scope.myCalendar.fullCalendar( 'addEventSource', $scope.events); 
		/* config object */
		vm.uiConfig = {
		
		  calendar:{
			
			defaultView:'agendaWeek',   //Ser� mostrada a vers�o de agenda semanal do usuario na tela incial.
			header:{
			  left: 'title',
			  center: '',
			  right: 'agendaDay,agendaWeek,month prev,today,next'
			},
			buttonText:{
				week:     'Agenda Semanal',
				day:      'Agenda de Hoje',
				month: 'Agenda Mensal'
			},
			dayNames:['Domingo', 'Segunda-feira', 'Terca-feira', 'Quarta-feira','Quinta-feira', 'Sexta-feira', 'Sabado'],
			lang: 'pt-br',
			height: 'auto',			
			timeFormat: 'HH:mm',
			axisFormat: 'HH:mm',
			editable: true,   
			weekends:false,             //Trabalha fins de semana. (A ser configurado pelo usuario)
			allDaySlot:false,
			titleFormat:"DD MMMM",       // Formato de Data do Titulo
			columnFormat:"dddd / DD",      // Formato de Hora do na Coluna
			minTime:"08:00:00",         // Hora que ir� come�ar o calendario (Uma hora antes do inicio do expediente)
			maxTime:"19:00:00",         // Hora que ir� terminar o calendario. (Duas horas depois do inicio do expediente)
			selectable: true,           // Permite selecionar um range de horas/datas
			selectHelper: true,
			unselectAuto:true,
			droppable:true,
			timezone:"local",
			defaultTimedEventDuration:"00:15:00", // Default dura��o para eventos que n�o possuem o periodo de termino setado.
			forceEventDuration:true,    // Ir� for�ar uma data/hora final para eventos sem este atributo. (Ir� usar o atributo defaultTimedEventDuration/defaultAllDayEventDuration para calculo )
			slotDuration:"00:15:00",    // Intervalos que ser� poss�vel agendar uma consulta. (A ser configurado pelo usuario)
			snapDuration:"00:15:00",    // Dura��o de uma consulta. (A ser configurado pelo usuario)
			businessHours:{             // Horario do expediente. (A ser configurado pelo usuario)
				start: '09:00',         // Hora de incio (A ser configurado pelo usuario)
				end: '18:00',           // Fim de expediente (A ser configurado pelo usuario)
				dow: [ 1, 2, 3, 4, 5 ]  //Dias da semana do expediente. (A ser configurado pelo usuario)
			},
			//Selecionando um novo evento em uma data n�o clicada. ( Dia ou Sele��o )
			select: function(start,end){
				inserirEvento(start,end);
			},
			eventResize: function(event, delta, revertFunc, jsEvent, ui, view ){
				var ask = confirm("Deseja realmente remarcar este evento?");
				if(ask){
					// Atualizar novo hor�rio.
				}else{
					revertFunc();
				}
			},
			eventDrop: function( event, delta, revertFunc, jsEvent, ui, view ){
				var ask = confirm("Deseja realmente remarcar este evento?");
				if(ask){
					// Atualizar novo hor�rio.
				}else{
					revertFunc();
				}
			},
			eventClick: function(event){
				//editarEventoDiaClicado(event);
			}
		  }
		};

		function inserirEvento(start,end) {
			var modalInstance = $modal.open({
				animation:true,
				size:"lg",
				templateUrl: 'app/modalagenda/modal.agenda.novoevento.html',
				controller: 'ModalAgendaNovoEventoController',
				controllerAs: 'vm',
				bindToController:true,
				resolve: {
					dataInicio: function () {
					  return start;
					},
					dataFim: function () {
					  return end;
					}
				}
			});
			modalInstance.result.then(function (novoEvento) {
			  dbEventos.push(novoEvento); 
			}, function () {
				//Nenhum Selecionado.
			});
		};
		
		function editarEventoDiaClicado(date) {
			var modalInstance = $modal.open({
				animation:true,
				size:"lg",
				templateUrl: 'app/modalagenda/modal.agenda.novoevento.html',
				controller: 'ModalAgendaNovoEventoController',
				controllerAs: 'vm',
				bindToController:true,
				resolve: {
					dataSelecionada: function () {
					  return date;
					}
				}
			});
			modalInstance.result.then(function (novoEvento) {
			  novosEventos.push(novoEvento);
			}, function () {
			  alert("SEM PARAMETRO");
			});
		};
		
		//Ao montar a lista de array ser� necessario iniciar inserindo pelo os mais proximos da data atual.
		//Ira agilizar o processo de delete.
		function apagarEvento(evento) {
			
			for(var i = 0; i < dbEventos.length; i++){

				if(evento.id == dbEventos[i].id){
					 dbEventos.splice(i,1);
					 break;
				}
				
			}
			
		};
    
	}

})();

