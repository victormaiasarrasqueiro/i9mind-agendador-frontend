﻿(function() {
    'use strict';

    angular.module('app.cadastrobase').controller('CadastrarAlterarProfissionalController', CadastrarAlterarProfissionalController);

	CadastrarAlterarProfissionalController.$inject = ['$scope', '$q', 'profissional', 'CRUDService', 'UtilService', 'TabelaService', 'ValidateService', 'AlertService','DateUtilService','$state'];

    function CadastrarAlterarProfissionalController($scope, $q, profissional, CRUDService, UtilService, TabelaService, ValidateService, AlertService, DateUtilService,$state) {
        
		var vm = this;
		
		vm.pagTitle = "";
		
		// Metodos publicos - Manter Profissional
		vm.alterar = alterar;
		vm.cancelar = cancelar;
		vm.inserir = inserir;
		vm.remover = remover;
		
		// Metodos públucos - Manter Períodos e Especialidades do profissional
		vm.insPrd = insPrd;
		vm.excPrd = excPrd;
		vm.insEsp = insEsp;
		vm.excEsp = excEsp;
		
		// Objeto
		vm.obj = {};
		vm.obj.nmPro = "";
		vm.obj.tpJtr = "1";              // tipoJornadaTrabalho - Tipo de Jornada de trabalho do profissional (1-Tradicional ou 2-Personalisado )
		vm.obj.ltPrd = [];		         // listaPeriodo - Lista de Horários habilitados para o profissional.
		vm.obj.ltEsp = [];               // listaEspecialidade - Lista de Especialidades habilitados para o profissional.
		vm.obj.objCon = {};
		vm.obj.objCon.nuCel = "";
		
		// Novos Objetos.
		vm.newEsp = {};  				 // Objeto de Cadastro de Especialidade
		
		vm.newPeriodoObj = {};           // Objeto de Cadastro de Periodo de data.
		vm.newPeriodoObj.ltDay = [];
		vm.newPeriodoObj.start = moment("2000-01-01T07:30:00");
		vm.newPeriodoObj.end   = moment("2000-01-01T17:30:00");
			
		// Listas auxiliares.
		vm.ltRangePrd = [];              // listaRangePeriodos - Lista de range de periodos ( hora inicio - hora fim ) atuais do profissional.
										 // * Usado como lista de objetos range do moment, para facilitar na validação de periodos de trabalho.

		vm.week = [];                    // Lista de dias da semana a ser exibida na combobox para serem selecionadas para o profissional.
		vm.listaEsp = [];                // Lista de Especialidades a serem cadastradas para o profissional.
		vm.ltTpSex = [];                 // Lista de Tipos de Sexo - Masculino/Feminino
		
		//Configuração do timepicker
		vm.hstep = 1;                    
		vm.mstep = 15;
		
		// Eventos de Página.
		vm.showInsPrd = false;           // Mostrar cadastro de Períodos.
		vm.showInsEsp = false;           // Mostrar cadastro de Especialidades.
		
		

		/*
		* activate()
		*
		* Função Inicio.
		*/
		function activate() {

			if(profissional != null){
				
				vm.operation = 2;
				vm.pagTitle = "Alterar Profissional";
				vm.obj = angular.copy(profissional);
				vm.obj.objEnd.dsLog = "frferferferf";
				formatarDataRetorno();
	
			}else{
				
				vm.operation = 1;
				vm.pagTitle = "Cadastrar Profissional";
				restartNewObject();
				
			};

			var promises = [getWeek(),listarEsp(),getListaTpSex()];
			return $q.all(promises);

        };
	
		/*
		* inserirPeriodo()
		*
		* Inseririndo um novo período na grande do profissional.
		*/
		function insPrd(){

			//Verifica se a lista de dias da semana selecionados durante a insersao de periodos é maior que zero.
			if(vm.newPeriodoObj.ltDay.length > 0){
			
				//Percorre a lista de dias da semana selecionados, criando objetos com a hora escolhida
				for(var i=0;i<vm.newPeriodoObj.ltDay.length;i++){

					var periodo = {'idDay':vm.newPeriodoObj.ltDay[i],'start':vm.newPeriodoObj.start,'end':vm.newPeriodoObj.end};
				
					if(testPrdSel(periodo)){
						vm.obj.ltPrd.push(periodo);
					};
					
				};
				
				vm.showInsPrd = false;
				atualizarRangeListaPeriodo();
			};
	
		};

		/*
		*  excluirPeriodo()
		*
		*  Excluir um periodo cadastrado para este profissional.
		*/
		function excPrd(id) {

			vm.ltRangePrd = UtilService.removeObjectList(id,"id",vm.ltRangePrd);
			vm.obj.ltPrd  = UtilService.removeObjectList(id,"id",vm.obj.ltPrd);
	
			atualizarRangeListaPeriodo();
			
		};
		
		/*
		* inserirEspecialidade()
		*
		* Inseririndo uma nova especialidade para o profissional.
		*/
		function insEsp(){
			
			if(UtilService.contains(vm.newEsp.id,"id",vm.obj.ltEsp)){
				AlertService.toastrErro('Duplicidade','Especialidade já cadastrada para este profissional.');
			}else{
				vm.obj.ltEsp.push(vm.newEsp);
				vm.newEsp = [];
				vm.showInsEsp = false;
			};
			
		};
		
		/*
		* excluirEspecialidade()
		*
		* Excluindo uma especialidade para o profissional.
		*/
		function excEsp(id){
			vm.obj.ltEsp  = UtilService.removeObjectList(id,"id",vm.obj.ltEsp);
		};
	
		/*
		* testarPeriodoelecionado()
		*
		* Validando se o periodo selecionado é válido.
		*/
		function testPrdSel(periodo){
			
			//Validando se o periodo selecionado ja foi incluido antes.
			var newRange = moment.range(  periodo.start ,  periodo.end );
			newRange.idDay = periodo.idDay;
			
			if(DateUtilService.isHoraEndMenorStart(newRange)){
				AlertService.toastrErro('Inválido' , 'A hora final não pode ser igual ou antes da hora inicial. (' + moment(newRange.start).hour() + ':' +   moment(newRange.start).minute() + ' - ' +  moment(newRange.end).hour() + ':' +   moment(newRange.end).minute() + ')');
				return false;
			}else if(DateUtilService.isPeriodoSobrepondoOutros(newRange,vm.ltRangePrd)){
				AlertService.toastrErro('Duplicado','O período informado (' + moment(newRange.start).hour() + ':' +   moment(newRange.start).minute() + ' - ' +  moment(newRange.end).hour() + ':' +   moment(newRange.end).minute() + ') esta sobrepondo outro período já cadastrado');
				return false;
			};

			return true;
		};

		/*
		* atualizarRangeListaPeriodo()
		*
		* Atualiza uma variavel com uma lista de ranges dos periodos selecionados para o profissional.
		*/
		function atualizarRangeListaPeriodo(){
			
			vm.obj.ltPrd = UtilService.enumArray(vm.obj.ltPrd);
			
			vm.ltRangePrd = [];
			
			for(var i=0; i<vm.obj.ltPrd.length; i++){
				var newRange = moment.range(moment(vm.obj.ltPrd[i].start),moment(vm.obj.ltPrd[i].end));
				newRange.idDay = vm.obj.ltPrd[i].idDay;
				newRange.id = vm.obj.ltPrd[i].id;
				vm.ltRangePrd.push(newRange);
			};
			
		};

		/*
		* listarEsp()
		*
		* Listando todas as Especialidades Cadastradas na Base de Dados.
		*/
		function listarEsp(){
		
			return CRUDService.especialidade().listar().then(function(data) {
				vm.listaEsp = data;
				return data.object;
			});

		};
	
		/*
		* inserir()
		*
		* Inseririndo Novo Registro na Base de Dados.		
		*/
		function inserir(){

			if(validate()){
			
				tratarCamposCliente();
			
				return CRUDService.profissional().inserir(vm.obj).then(function(data) {
					formatarDataRetorno();
					AlertService.sweetSucesso(1,"O profissional", redirecionarParaListagem);
					return data;
				});
				
			};

		};

		/*
		* alterar()
		*
		* Alterando o obj selecionado.
		*/
		function alterar(){

			if(validate()){
				
				tratarCamposCliente();
				
				return CRUDService.profissional().atualizar(vm.obj).then(function(data) {
					formatarDataRetorno();
					AlertService.sweetSucesso(2,"O profissional", redirecionarParaListagem);
					return data;
				});
				
			};
		};
		
		//Tela de Edição do cliente.
		function remover(idC) {
			return CRUDService.profissional().remover(idC).then(function(data) {
				AlertService.sweetSucesso(3,"O profissional", redirecionarParaListagem);
				return data;
			});
		};
		
		/*
		* cancelar()
		*
		* Cancelando a operação de inclusão ou alteração.
		*/
		function cancelar(){
			vm.obj = {};
			vm.isExibeCadastrar = false;
			vm.isExibeAlterar = false;
		};
		
		/*
		* validate()
		*
		* Validando os campos antes de serem enviados.
		*/
		function validate(){

			if(ValidateService.isStrEmpty(vm.obj.nmPro)){
				AlertService.toastrErro('Campo Obrigatório','É preciso informar o nome do profissional.');
				return false;
			};
			
			if(ValidateService.isStrEmpty(vm.obj.objCon.nuCel)){
				AlertService.toastrErro('Campo Obrigatório','É preciso informar o celular do profissional.');
				return false;
			};
			
			if(ValidateService.isListEmpty(vm.obj.ltEsp)){
				AlertService.toastrErro('Campo Obrigatório','É preciso selecionar alguma especialidade para este profissional.');
				return false;
			};
			
			if(ValidateService.isListEmpty(vm.obj.ltPrd)){
				AlertService.toastrErro('Campo Obrigatório','É preciso selecionar algum período de trabalho para este profissional.');
				return false;
			};
			
			//Validando se já existe um registro ATIVO com as mesmas caracteristicas.
			if(ValidateService.hasDuplicidade(vm.obj.nmEsp,"nmEsp",vm.lista)){
				AlertService.toastrErro('Duplicidade','Já existe uma especialidade cadastrada com este nome.');
				return false;
			};

			return true;

		};

		/*
		* getWeek()
		*
		* Listando todos os dias da semana.
		*/
		function getWeek(){
		
			return DateUtilService.getWeek().then(function(data) {
			
				vm.week = data;
				
				return data;
			});

		};	
		
		/*
		* getListaTpSex()
		*
		* Listando todos os Tipos de Sexo.
		*/
		function getListaTpSex(){
		
			return TabelaService.getListaTpSex().then(function(data) {
			
				vm.ltTpSex = data;
				
				return data;
			});

		};	
			
		function restartNewObject(){
			
			vm.obj.tpSex = "0";
			vm.obj.idOEU = "0";
			vm.obj.tpECi = "0";
			vm.obj.idOCe = "0";

		};
		
		// Tratando campos antes de incluir/alterar
		function tratarCamposCliente(){
			
			/* Tratando as datas da página*/
			vm.obj.dtNas = DateUtilService.dateStrToIsoStr(vm.obj.dtNas);

		};
		
		// Formatando as datas vindas do banco, para o formato de tela ( mask )
		function formatarDataRetorno(){
		
			vm.obj.dtNas = DateUtilService.isoStrToFormatDateStr(vm.obj.dtNas);

		};
		
		//Redirecionando para a página de listagem de clientes.
		function redirecionarParaListagem(){
			$state.transitionTo("app.profissional", {});
		};
		
		/*
		*
		* Iniciando
		*
		*/
		activate();
		
	};

})();

