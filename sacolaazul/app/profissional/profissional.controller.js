(function() {
    'use strict';

    angular.module('app.profissional').controller('ProfissionalController', ProfissionalController);

	ProfissionalController.$inject = ['$scope', '$state', 'CRUDService','crudList','DTDefaultOptions','DTOptionsBuilder', 'AlertService'];

    function ProfissionalController($scope, $state, CRUDService,crudList,DTDefaultOptions,DTOptionsBuilder,AlertService) {
        
		var vm = this; 							// vm = view-model
		vm.edit = edit;

		vm.lista = [];                      	// Lista de profissionais.
		
		vm.dtInstance = {};
		
		vm.dtOptions = DTOptionsBuilder
			.newOptions()
			.withOption('destroy', true)
			.withColReorder()
			.withBootstrap()
			.withBootstrapOptions({
				pagination: {
					classes: {
						ul: 'pagination pagination-sm'
					}
				}
			})
			.withOption('responsive', true);
			
		/*
		* Fun��o Inicio.
		*/
		function activate() {
			//Listando os registros cadastrados
			vm.lista = crudList;
        };
		
		// Tela de Edi��o do cliente.
		function edit(idC) {
			$state.transitionTo("app.alterarProfissional", {id:idC});
		};


		/* Tela de Edi��o do cliente.
		function listar(idC) {
			alert("listar");
			return CRUDService.profissional().listar().then(function(data) {
				vm.lista = data;
				reloadData();
				return data;
			});
			
		};
		*/

		activate();
	}

})();

