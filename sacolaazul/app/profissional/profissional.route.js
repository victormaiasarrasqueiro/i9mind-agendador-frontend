(function() {

    'use strict';

    angular.module('app.profissional').run(appRun);

    appRun.$inject = ['routerHelper','CRUDService'];

    function appRun(routerHelper,CRUDService) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        
		return [
		
		{
            state: 'app.cadastrarProfissional',
            config: {
                url: '/profissional/cadastrar',
                title: 'Cadastrar Novo Profissional',
                views: {
                    'content@app': {
                        templateUrl: 'app/profissional/cadastrar-alterar-profissional.html',
                        controller: 'CadastrarAlterarProfissionalController',
                        controllerAs: 'vm'
                    }
                },
				resolve:{

					profissional: function(){

						return null;
					}
					
				},
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        },
		{
            state: 'app.alterarProfissional',
            config: {
                url: '/profissional/alterar/:id',
                title: 'Alterar Profissional',
                views: {
                    'content@app': {
                        templateUrl: 'app/profissional/cadastrar-alterar-profissional.html',
                        controller: 'CadastrarAlterarProfissionalController',
                        controllerAs: 'vm'
                    }
                },
				resolve:{

					profissional: function($stateParams,CRUDService){
						
						return CRUDService.profissional().obter($stateParams.id).then(function(data){ return data; });
						
					}
					
				},
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        },
		{
            state: 'app.profissional',
            config: {
                url: '/profissional',
                title: 'Profissionais',
                views: {
                    'content@app': {
                        templateUrl: 'app/profissional/profissional.html',
                        controller: 'ProfissionalController',
                        controllerAs: 'vm'
                    }
                },
				resolve:{

					crudList: function(CRUDService){

						return CRUDService.profissional().listar().then(function(data) {
							return data;
						});
						
					}
					
				},
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        }];
    }

})();
