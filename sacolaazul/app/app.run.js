(function() {
    'use strict';
	
	//Adcionando na valida��o o estilo do Bootstrap CSS
	angular.module('app').run([
        'bootstrap3ElementModifier',
        function (bootstrap3ElementModifier) {
              bootstrap3ElementModifier.enableValidationStateIcons(true);
       }]);
	   
angular.module('jcs-autoValidate')
.run([
    'defaultErrorMessageResolver',
    function (defaultErrorMessageResolver) {
        // To change the root resource file path
        //defaultErrorMessageResolver.setI18nFileRootPath('some/path);
		defaultErrorMessageResolver.setI18nFileRootPath("app/i18n/angular-auto-validate/lang"); 
        defaultErrorMessageResolver.setCulture("pt-br");
    }
]);

angular.module('app')
            .run([
                'validator',
                function (validator) {
                    validator.setValidElementStyling(false);
                  
            }]);
	   
})();


angular.module('app').config(function(toastrConfig) {
  angular.extend(toastrConfig, {
    autoDismiss: false,
    containerId: 'toast-container',
    maxOpened: 5,    
    newestOnTop: true,
	progressBar: true,
	tapToDismiss:true,
	closeButton:true,
	allowHtml:true,
    positionClass: 'toast-top-right',
    preventDuplicates: false,
    preventOpenDuplicates: true,
    target: 'body'
  });
});