(function() {
    'use strict';

    angular.module('alertasimples.directive', []);

	angular.module('alertasimples.directive').directive('alertasimples',['$timeout',function($timeout) {
	
	return {
		restrict: 'E',
		replace: false,
		transclude: true,
		scope: {
			tipo: '=tipo'
		},
		templateUrl: "app/directives/alertasimples/alertasimples.html",
		link: function(scope, element, attrs, controller, transcludeFn) {
			$timeout(function() {
				element.empty();
				element.hide();
				element.remove();
			}, 3000);
		}
	};

  }]);

})();