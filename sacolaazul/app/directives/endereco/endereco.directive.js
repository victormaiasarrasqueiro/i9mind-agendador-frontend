(function() {
    'use strict';

    angular.module('endereco.directive', []);

	angular.module('endereco.directive').directive('endereco',['EnderecoService',function(EnderecoService) {
	
	return {
      
		templateUrl:'app/directives/endereco/endereco.html',
      
		restrict: 'E',
      
		scope: {
			obj: "="
		},

		link: function(scope) {

		},
		controller:function($scope,EnderecoService){

		$scope.endExibirCep = false;
			$scope.shouldBeOpen = false;
			$scope.endPesqCep = "";
			
			//Iniciando Google Address
			$scope.endAddress = {};
			$scope.endAddress.components = {};
			$scope.endAddress.place = {};
		
			$scope.endGoogleOptions = {
			  types: ['address'],
			  componentRestrictions: { country: 'BR' }
			};
			
			$scope.endExibirBuscaCep = function(){
				$scope.endExibirCep = ! $scope.endExibirCep;
				$scope.shouldBeOpen = $scope.endExibirCep;
			};
			
			$scope.endExibirBuscaCep = function(){
				$scope.endExibirCep = ! $scope.endExibirCep;
				$scope.shouldBeOpen = $scope.endExibirCep;
			};

			$scope.endCarregarCep = function(){		
			
				return EnderecoService.getEnderecoCep($scope.endPesqCep).then(function(data) {
					
					var endRet = data.data;
					
					$scope.obj.dsLog = endRet.logradouro;
					$scope.obj.dsBai = endRet.bairro;
					$scope.obj.dsCid = endRet.localidade;
					$scope.obj.dsEst = endRet.uf;
					$scope.obj.nuCep = endRet.cep;
					$scope.obj.dsPai = "Brasil";
					
					return data;
					
				});
			};
			
			//Localizar o endere�o pelo Google.
			$scope.$watch('endAddress.place', function() {

				angular.forEach($scope.endAddress.place.address_components, function(value, key) {

					switch(value.types[0]) {
						
						case "route":
							$scope.obj.dsLog = value.short_name;
							break;
						
						case "street_number":
							$scope.obj.nuLog = value.short_name;
							break;
							
						case "neighborhood":
							$scope.obj.dsBai = value.short_name;
							break;
							
						case "sublocality_level_1":
							$scope.obj.dsBai = value.short_name;
							break;
						
						case "locality":
						
							$scope.obj.dsLoc = value.short_name;
							break;
							
						case "administrative_area_level_2":
							$scope.obj.dsCid = value.short_name;
							break;
							
						case "administrative_area_level_1":
							$scope.obj.dsEst = value.short_name;
							break;
							
						case "country":
							$scope.obj.dsPai = value.long_name;
							break;
							
						case "postal_code":
							$scope.obj.nuCep = value.long_name;
							break;
					}

				});

			});
			
		}

    };

  }]);

})();