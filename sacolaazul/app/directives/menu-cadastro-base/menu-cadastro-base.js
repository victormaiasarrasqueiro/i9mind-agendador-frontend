(function() {
    'use strict';

    angular.module('menucadastrobase.directive', []);

	angular.module('menucadastrobase.directive').directive('menucadastrobase',['$location',function() {
	
	return {
      
		templateUrl:'app/directives/menu-cadastro-base/menu-cadastro-base.html',
		restrict: 'E',
		scope: {
		
		},
		link: function(scope) {
		}

    };

  }]);

})();