(function() {
    'use strict';

    angular.module('widget.directive', []);
	angular.module('widget.directive').directive('widget',function(){
		return {
		  templateUrl:'app/directives/widget/widget.html',
		  restrict: 'E',
		  replace: true,
		  scope: {
		    id: '@',
			name: '@',
			complemento: '@'
		  },
		  link: function(scope, element, attrs, controller, transcludeFn) {
		  },
		  controller:function($scope){
		  },
		  transclude:true
		}
	});
})();
