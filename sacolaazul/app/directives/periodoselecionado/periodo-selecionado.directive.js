(function() {
    'use strict';

    angular.module('periodo.selecionado.directive', []);
	angular.module('periodo.selecionado.directive').directive('periodoSelecionado',function(){
		return {
		  templateUrl:'app/directives/periodoselecionado/periodo-selecionado.html',
		  restrict: 'E',
		  replace: true,
		  scope: {
		  },
		  link: function(scope, element, attrs, controller, transcludeFn) {
		  },
		  controller:function($scope){
			
		  },
		  transclude:true
		}
	});
})();
