(function() {
    'use strict';

    angular.module('header.notification.directive', ['ui.bootstrap']);
	
	angular.module('header.notification.directive').directive('headerNotification',function(){
		return {
        templateUrl:'app/directives/header/header-notification/header-notification.html',
        restrict: 'E',
        replace: true,
		controller:function($scope){
		}
    	}
	});
})();


