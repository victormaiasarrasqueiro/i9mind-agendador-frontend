(function() {
    'use strict';

    angular.module('header.directive', []);

	angular.module('header.directive').directive('header',function(){
		return {
        templateUrl:'app/directives/header/header.html',
        restrict: 'E',
        replace: true,
    	}
	});
})();


