(function() {
    'use strict';

    angular.module('contato.directive', []);

	angular.module('contato.directive').directive('contato',['TabelaService','$q',function(TabelaService,$q) {
	
	return {
      
		templateUrl:'app/directives/contato/contato.html',
      
		restrict: 'E',
      
		scope: {
			obj: "="
		},

		link: function(scope) {

		},
		controller:function($scope,TabelaService,$q){
			
			$scope.ltOCel = [];
			
			/*
			* activate()
			*
			* Fun��o Inicio.
			*/
			function activate() {
				
				if(angular.isUndefined($scope.obj)){
					$scope.obj = {};
					$scope.obj.idOCe = "0";
				}else if(angular.isUndefined($scope.obj.idOCe) || $scope.obj.idOCe == null){
					$scope.obj.idOCe = "0";
				};

				var promises = [getListaOCel()];
				return $q.all(promises);

			};

			/*
			* getListaOCel()
			*
			* Listando todos as operadoras de celular.
			*/
			function getListaOCel(){
			
				return TabelaService.getListaOCel().then(function(data) {
				
					$scope.ltOCel = data;
					
					return data;
				});

			};	
			
			
			/*
			*
			* Iniciando
			*
			*/
			activate();
			
		}

    };

  }]);

})();