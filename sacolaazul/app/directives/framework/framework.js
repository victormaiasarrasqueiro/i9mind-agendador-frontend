(function() {
    'use strict';
	
	// Code goes here
	angular.module('framework', ['ui.bootstrap'])
	angular.module('framework').directive('fmk-datepicker',function(){

	  return {
		restrict: "E",
		scope:{
		  ngModel: "=",
		  dateOptions: "=",
		  opened: "=",
		},
		link: function($scope, element, attrs) {
		  
			$scope.openCalendar = function(e, date) {
				e.preventDefault();
				e.stopPropagation();
				$scope.open[date] = true;
			}

			$scope.clear = function () {
				$scope.ngModel = null;
			};

		},
		templateUrl: 'app/directives/framework/fmk-datepicker/fmk-datepicker.html'
	  }
	})

})();
