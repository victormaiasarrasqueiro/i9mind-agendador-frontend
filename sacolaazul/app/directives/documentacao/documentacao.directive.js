(function() {
    'use strict';

    angular.module('documentacao.directive', []);

	angular.module('documentacao.directive').directive('documentacao',['$q','EnderecoService','TabelaService',function($q,EnderecoService,TabelaService) {
	
	return {
      
		templateUrl:'app/directives/documentacao/documentacao.html',
      
		restrict: 'E',
      
		scope: {
			obj: "="
		},

		link: function(scope) {

		},
		controller:function($scope,$q,EnderecoService,TabelaService){
			
			$scope.listaUF = [];
			$scope.listaTpECivil = [];
			
			/*
			* activate()
			*
			* Fun��o Inicio.
			*/
			function activate() {

				var promises = [getListaUF(),getListaTpECivil()];
				return $q.all(promises);

			};
			
			/*
			* getListaUF()
			*
			* Listando todas as UF's cadastradas.
			*/
			function getListaUF(){
			
				return EnderecoService.getListaUF().then(function(data) {
				
					$scope.listaUF = data;
					
					return data;
				});

			};	
			
			/*
			* getListaTpECivil()
			*
			* Listando todos os tipos de estados civis
			*/
			function getListaTpECivil(){
			
				return TabelaService.getListaTpECivil().then(function(data) {
				
					$scope.listaTpECivil = data;
					
					return data;
				});

			};
			
			/*
			*
			* Iniciando
			*
			*/
			activate();

			
		}

    };

  }]);

})();