(function() {
    'use strict';

    angular.module('app.directives', [
		'ngSanitize',
		'header.directive',
		'header.notification.directive',
		'sidebar.directive',
		'widget.directive',
		'widget-title-top.directive',
		'sidebar.search.directive',
		'focus.me.directive',
		'ui.select',
		'minicolors',
		'calendario.directive',
		'alertasimples.directive',
		'endereco.directive',
		'contato.directive',
		'documentacao.directive',
		'menucadastrobase.directive',
		'framework'
	]);
	
})();
