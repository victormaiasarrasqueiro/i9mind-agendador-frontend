(function() {
    'use strict';

    angular.module('widget-title-top.directive', []);

	angular.module('widget-title-top.directive').directive('widgetTitleTop',function(){
		return {
			templateUrl:'app/directives/widget-title-top/widget-title-top.html',
			restrict: 'E',
			replace: true,
			transclude:true
		}
	});

})();
