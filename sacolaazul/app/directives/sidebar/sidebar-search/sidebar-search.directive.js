(function() {
    'use strict';

    angular.module('sidebar.search.directive', []);
	
	angular.module('sidebar.search.directive').directive('sidebarSearch',function(){
    return {
      templateUrl:'app/directives/sidebar/sidebar-search/sidebar-search.html',
      restrict: 'E',
      replace: true,
      scope: {
      },
      controller:function($scope){
        $scope.selectedMenu = 'home';
      }
    }
  });
	
	
})();

 

