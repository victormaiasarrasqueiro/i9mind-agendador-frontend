/**
 * Created by gabriel.lima on 08/06/2015.
 */


(function() {
    'use strict';

    angular.module('app.directives').directive('wdevGroupbuttions', function($timeout) {

        return {
            restrict: 'A',
            link: function(scope, element) {

                // We use a tomeout of 0ms so that that semantic waits for angular to init the scope
                $timeout(function() {


                    var handler = {

                        activate: function() {
                            $(this)
                                .addClass('active')
                                .siblings()
                                .removeClass('active')
                            ;
                        }

                    };

                    element.on('click', handler.activate);

                });

            }
        }

    });
})();