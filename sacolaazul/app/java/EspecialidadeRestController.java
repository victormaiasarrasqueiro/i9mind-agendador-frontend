package com.i9mind.agendador.integration.service.rest.controller;


import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.i9mind.agendador.business.entity.Cliente;
import com.i9mind.agendador.business.entity.Especialidade;
import com.i9mind.agendador.business.service.ClienteService;
import com.i9mind.agendador.comum.util.auxiliary.StringUtils;


@RestController
@RequestMapping("/especialidade")
public class EspecialidadeRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EspecialidadeRestController.class);
	 
	@Autowired
	private ClienteService clienteService;

	/**
	 * @name     inserirEspecialidade
	 * 
	 * @method   POST
	 * @param    Especialidade pEspecialidade
	 * @return   ResponseEntity<String>
	 * @produces application/JSON
	 * @info     Inserir uma nova especialidade no sistema.
	*/
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/inserirEspecialidade", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> inserirEspecialidade(@Valid @RequestBody(required=true) Especialidade pEspecialidade, BindingResult bindingResult) {
    	
		try
		{
			
	        if (bindingResult.hasErrors()) {
	        	return new ResponseEntity<Object>(HttpStatus.PRECONDITION_FAILED);
	        }
	        
	        // Inserindo o objeto.
	        String id = clienteService.inserirCliente(pCliente);
	    	Cliente c = new Cliente();
	    	c.setId(id);
	        
	    	return new ResponseEntity<Cliente>(c, HttpStatus.OK);

		}
		catch(Exception e)
		{
			LOGGER.error("[ClienteRestController - inserirCliente]   MSG: " + e );
			return new ResponseEntity<Cliente>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	};
	
	/**
	 * @name     atualizarCliente
	 * 
	 * @method   PUT
	 * @param    Cliente pCliente
	 * @return   ResponseEntity
	 * @produces application/JSON
	 * @info     Atualizar um objeto j� existente no sistema
	*/
	@RequestMapping(value = "/atualizarCliente", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> atualizarCliente(@Valid @RequestBody(required=true) Cliente pCliente, BindingResult bindingResult) {
    	
		try
		{
			
			if (bindingResult.hasErrors()) {
	        	return new ResponseEntity<Object>(HttpStatus.PRECONDITION_FAILED);
	        }
	        
	        // Atualizando o objeto
	        clienteService.atualizarCliente(pCliente);
	    	
	    	return new ResponseEntity<Object>(HttpStatus.OK);
	    
		}
		catch(Exception e)
		{
			LOGGER.error("[ClienteRestController - atualizarCliente]   MSG: " + e );
			return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	};
	
	/**
	 * @name     removerCliente
	 * 
	 * @method   DELETE
	 * @param    String id
	 * @return   ResponseEntity
	 * @produces application/JSON
	 * @info     Apagar um objeto j� existente no sistema
	*/
	@CrossOrigin(origins = "*")
    @RequestMapping(value = "/removerCliente/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<Object> removerCliente(@PathVariable String id) {
    	
		try
		{
			
	    	if(StringUtils.isEmpty(id)){
	    		return new ResponseEntity<Object>(HttpStatus.PRECONDITION_FAILED);
	    	}
	        
	        // Tornando o cliente INATIVO para a empresa.
	        clienteService.inativarCliente(id);
	    	
	    	return new ResponseEntity<Object>(HttpStatus.OK);
	    
		}
		catch(Exception e)
		{
			LOGGER.error("[ClienteRestController - removerCliente]   MSG: " + e );
			return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	};
     
	/**
	 * @name     listarCliente
	 * 
	 * @method   GET
	 * @return   ResponseEntity<List<Object>>
	 * @produces application/JSON
	 * @info     Retornar uma lista de objetos cadastrados.
	*/
	@CrossOrigin(origins = "*")
    @RequestMapping(value = "/listarCliente", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Cliente>> listarCliente() {
    	
    	try
		{
	    	return new ResponseEntity<List<Cliente>>(clienteService.listarCliente(), HttpStatus.OK);
		}
    	catch(Exception e)
    	{
			LOGGER.error("[ClienteRestController - listarCliente]   MSG: " + e );
			return new ResponseEntity<List<Cliente>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	
	};
	
	/**
	 * @name     recuperarClienteRemovido
	 * 
	 * @method   PUT
	 * @param    Cliente pCliente
	 * @return   ResponseEntity
	 * @produces application/JSON
	 * @info     Recupera um cliente deletado do sistema
	*/
	@RequestMapping(value = "/recuperarClienteRemovido", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> recuperarClienteRemovido(@Valid @RequestBody(required=true) Cliente pCliente, BindingResult bindingResult) {
    	
		try
		{
			
			if (bindingResult.hasErrors()) {
	        	return new ResponseEntity<Object>(HttpStatus.PRECONDITION_FAILED);
	        }
	        
	        // Atualizando o objeto
	        clienteService.recuperarCliente(pCliente);
	    	
	    	return new ResponseEntity<Object>(HttpStatus.OK);
	    
		}
		catch(Exception e)
		{
			LOGGER.error("[ClienteRestController - recuperarCliente]   MSG: " + e );
			return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	};
	
	/**
	 * @name     listarClienteRemovido
	 * 
	 * @method   GET
	 * @return   ResponseEntity<List<Object>>
	 * @produces application/JSON
	 * @info     Retornar uma lista de objetos removidos do sistema.
	*/
    @RequestMapping(value = "/listarClienteRemovido", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Cliente>> listarClienteRemovido() {
    	
    	try
		{
	    	// Recuperando a lista de objetos cadastrados
    		List<Cliente> lista = clienteService.listarClienteRemovido();
	    	
	    	return new ResponseEntity<List<Cliente>>(lista, HttpStatus.OK);
		}
    	catch(Exception e)
    	{
			LOGGER.error("[ClienteRestController - listarClienteRemovido]   MSG: " + e );
			return new ResponseEntity<List<Cliente>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	
	};

};