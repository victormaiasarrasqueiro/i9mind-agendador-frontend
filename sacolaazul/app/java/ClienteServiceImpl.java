package com.i9mind.agendador.business.service.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.i9mind.agendador.business.entity.Cliente;
import com.i9mind.agendador.business.repository.mongo.ClienteMongoRepository;
import com.i9mind.agendador.business.repository.mongo.crud.ClienteMongoCRUDRepository;
import com.i9mind.agendador.business.service.ClienteService;
import com.i9mind.agendador.comum.exception.IntegrationException;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class ClienteServiceImpl implements ClienteService{
	
	//private static final Logger LOGGER = LoggerFactory.getLogger(ClienteServiceImpl.class);
	
	@Autowired
	ClienteMongoRepository clienteMongoRepository;
	
	@Autowired
	ClienteMongoCRUDRepository clienteMongoCRUDRepository;
	
	public Cliente obterCliente(String id){
		
		try
		{
			Cliente clienteBusca = new Cliente();
			
			clienteBusca.setId(id);
			
			clienteBusca.setIdEmpresa(new BigInteger("1"));
			
			return clienteMongoRepository.obterCliente(clienteBusca);
		}
    	catch(Exception e)
    	{
			throw new IntegrationException("ClienteServiceImpl", "obterCliente" , "Erro ao obter o cliente pelo id informado [" + id + "]", e);
		}
		
	};

	/**
	 * Inserindo um novo cliente para a empresa logada.
	 */
	public String inserirCliente(Cliente pCliente){
		
		try
		{
			
			//Tratando os campos inseridos.
			tratarCampos(pCliente);
			
			
			//Recuperando o ID da empresa logada.
			pCliente.setIdEmpresa(new BigInteger("1"));
			
			//Recuperando o ID do usu�rio logado.
			pCliente.setIdUsuarioCadadastro(new BigInteger("1"));
			
			//Recuperando a data atual.
			pCliente.setDataCadastroSistema(new Date());
			
			//Definindo o status como true.
			pCliente.setAtivo(true);
			
			// 1- Incluir no MySql
			// 2- Recuperar ID MySql
			pCliente.setIdGeral(new BigInteger("15"));
			
			//Inserindo no MongoDB
			Cliente retorno = clienteMongoCRUDRepository.insert(pCliente);
			
			//Retornando ID MongoDB
			return retorno.getId();
			
		}
    	catch(Exception e)
    	{
			throw new IntegrationException("ClienteServiceImpl", "inserirCliente" , "Erro ao inserir cliente", e);
		}	
	
	};
	
	public void atualizarCliente(Cliente pCliente){

	};
	
	/**
	 * 
	 * Alterar o status do cliente de ATIVO para INATIVO.
	 * 
	 * @return List<Cliente> - Lista de clientes.
	 * 
	 */
	public void inativarCliente(String id){
		
		try
		{
			Cliente clienteBusca = new Cliente();
			clienteBusca.setId(id);
			
			//Recuperando o ID da empresa logada.
			clienteBusca.setIdEmpresa(new BigInteger("1"));
			
			//Obter Cliente
			Cliente clienteRecuperado = clienteMongoRepository.obterCliente(clienteBusca);
			
			//Inativando o cliente no MONGO
			clienteMongoRepository.inativarCliente(clienteRecuperado);
			
			//Inativando o cliente no MYSQL
			//instrucao.
		}
    	catch(Exception e)
    	{
			throw new IntegrationException("ClienteServiceImpl", "removerCliente" , "Erro ao remover cliente", e);
		}	
		
	};
	
	/**
	 * 
	 * Listar Todos os Clientes da empresa logada.
	 * 
	 * @return List<Cliente> - Lista de clientes.
	 * 
	 */
	public List<Cliente> listarCliente(){
		
		try
		{
			//Recuperando a lista de clientes da empresa.
			Cliente clienteBusca = new Cliente();
			clienteBusca.setIdEmpresa(new BigInteger("1"));
			
			List<Cliente> listaRetorno = clienteMongoRepository.buscarTodosClienteEmpresa(clienteBusca);
			
			return listaRetorno;
		}
		catch(Exception e)
    	{
			throw new IntegrationException("ClienteServiceImpl", "listarCliente" , "Erro ao listar tosdos os clientes da empresa.", e);
		}	
	};
	
	public void recuperarCliente(Cliente pCliente){
		
	};
	
	public List<Cliente> listarClienteRemovido(){
		return null;
	};
	
	/**
	 * Inserindo um novo cliente para a empresa logada.
	 */
	private void tratarCampos(Cliente pCliente){
		
		
	};

}