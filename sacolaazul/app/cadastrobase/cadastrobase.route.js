﻿/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.cadastrobase').run(appRun);

    appRun.$inject = ['routerHelper','CRUDService'];

    function appRun(routerHelper,CRUDService) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.cadastrobaseespecialidade',
				config: {
					url: '/cadastrobase/especialidade',
					title: 'Cadastro de Especialidades',
					views: {
						'content@app': {
							templateUrl: 'app/cadastrobase/cadastrobase-especialidade.html',
							controller: 'CadastroBaseEspecialidadeController',
							controllerAs: 'vm'
						}
					},
					resolve:{

						crudList: function(CRUDService){

							return CRUDService.especialidade().listar().then(function(data) {
								return data;
							});
							
						}
						
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
			},
			{
				state: 'app.cadastrobaseoperadorasaude',
				config: {
					url: '/cadastrobase/operadorasaude',
					title: 'Cadastro de Operadoras de Saúde',
					views: {
						'content@app': {
							templateUrl: 'app/cadastrobase/cadastrobase-operadorasaude.html',
							controller: 'CadastroBaseOperadoraSaudeController',
							controllerAs: 'vm'
						}
					},
					resolve:{

						crudList: function(CRUDService){

							return CRUDService.operadorasaude().listar().then(function(data) {
								return data;
							});
							
						}
						
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
			}
		
		];
    };

})();
