﻿(function() {
    'use strict';

    angular.module('app.cadastrobase').controller('CadastroBaseEspecialidadeController', CadastroBaseEspecialidadeController);

	CadastroBaseEspecialidadeController.$inject = ['$scope', 'CRUDService', 'crudList', 'UtilService', 'ValidateService', 'AlertService', 'toastr'];

    function CadastroBaseEspecialidadeController($scope, CRUDService, crudList, UtilService, ValidateService, AlertService, toastr) {
        
		var vm = this;
		
		// Nome da Funcionalidade - Será exibido no cabeçalho da tela.
		vm.crud = {"nmFunc":"Cadastro de Especialidades","btTxt":"Especialidade"};

		//Metodos publicos
		vm.inserir = inserir;
		vm.exibirAlterar = exibirAlterar;
		vm.alterar = alterar;
		vm.remover = remover;
		vm.cancelar = cancelar;
		
		vm.objeto = {};
		vm.lista  = [];
		
		//Propriedades de paginação
		vm.currentPage = 1;
		vm.itemsPerPage = 10;
		
		//Exibição na tela.
		vm.isExibeCadastrar = false;
		vm.isExibeAlterar = false;
		

		/*
		* Função Inicio.
		*/
		function activate() {
			
			//Listando os registros cadastrados
			vm.lista = crudList;
			
        };
		
		/*
		* listar()
		*
		* Listando todos os registros ativos.
		*/
		function listar(){
		
			return CRUDService.especialidade().listar().then(function(data) {
				vm.lista = data.object;
				return data.object;
			});

		};
		
		/*
		* inserir()
		*
		* Inseririndo Novo Registro na Base de Dados.		
		*/
		function inserir(){
			
			if(validate()){
			
				return CRUDService.especialidade().inserir(vm.objeto).then(function(data) {

					AlertService.sweetSucesso(1,"O registro");
					alert(data.id);
					vm.objeto.id = data.id;
					vm.objeto.isNew = true;     //Exibe label 'novo'.
				
					vm.lista.push( angular.copy(vm.objeto) );
					
					vm.isExibeCadastrar = false;
					vm.objeto = {};
					
					return res;
				
				});
				
			};

		};
		
		/*
		* exibirAlterar()
		*
		* Exibindo a tela de alterar recuperando o objeto selecionado.
		*/
		function exibirAlterar(id){

			vm.objeto = UtilService.getObjectFromList(id,"id",vm.lista);
			
			vm.isExibeCadastrar = false;
			vm.isExibeAlterar = true;
		
		};
		
		/*
		* alterar()
		*
		* Alterando o objeto selecionado.
		*/
		function alterar(){
			
			if(validate()){
				
				return CRUDService.especialidade().atualizar(vm.objeto).then(function(data) {
					
					AlertService.sweetSucesso(2,"O registro");
					
					vm.objeto.isNew = false;
					
					vm.lista = UtilService.updateObjectFromList(vm.objeto.id,"id",vm.objeto,vm.lista);
					
					vm.isExibeAlterar = false;
					vm.objeto = {};
					
					return data;
					
				});
				
			};
		};
		
		/*
		* remover()
		*
		* Removendo o obejeto
		*/
		function remover(id){
		
			return CRUDService.especialidade().remover(id).then(function(data,status) {
				
				AlertService.sweetSucesso(3,"O registro");
				
				vm.lista = UtilService.removeObjectList(id,"id",vm.lista);
				
				return data;

			});
		
		};
		
		/*
		* cancelar()
		*
		* Cancelando a operação de inclusão ou alteração.
		*/
		function cancelar(){
			vm.objeto = {};
			vm.isExibeCadastrar = false;
			vm.isExibeAlterar = false;
		};
		
		/*
		* validate()
		*
		* Validando os campos antes de serem enviados.
		*/
		function validate(){
			
			//Validando se já existe um registro ATIVO com as mesmas caracteristicas.
			if(ValidateService.hasDuplicidade(vm.objeto.nmEsp,"nmEsp",vm.lista)){
				toastr.error('Já existe uma especialidade cadastrada com este nome.', 'Duplicidade');
				return false;
			};

			return true;

		};
		
		
		// Iniciando o procedimento.
		activate();
		
	};

})();

