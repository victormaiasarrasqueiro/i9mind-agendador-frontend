﻿(function() {
    'use strict';

    angular.module('app.paciente').controller('PacienteController', PacienteController);

	PacienteController.$inject = ['$scope', '$filter', '$compile', '$state', 'DTDefaultOptions', 'DTOptionsBuilder', 'DTColumnBuilder', 'ClienteService', 'UtilService'];

    function PacienteController($scope, $filter, $compile, $state, DTDefaultOptions, DTOptionsBuilder, DTColumnBuilder, ClienteService, UtilService) {
        
		var vm = this;

		vm.edit = edit;
		vm.gerenciar = gerenciar;
		vm.dtInstance = {};

		//Função Inicio.
	  	function activate() {
			//vazio.
        };

		//Funcão responsável por recuperar uma promisse com a lista de pacientes do usuário.
		function getListaPaciente() {

			return ClienteService.listarCliente().then(function(retorno){
				return retorno.data;
			});
				
        };
		
		// Tela de Edição do cliente.
		function edit(idC) {
			$state.transitionTo("app.alterarCliente", {id:idC});
		}
		
		// Tela de Gerenciamento do cliente.
		function gerenciar(idC) {
			$state.transitionTo("app.gerenciarCliente", {id:idC});
        };

		//Atualizar os dados do datatable.
		function reloadData() {
			var resetPaging = true;
			vm.dtInstance.reloadData(callback, resetPaging);
		}
		
		function callback(json) {
			alert("Excluido com sucesso!!!");
		}

		vm.dtOptions = DTOptionsBuilder
			
			//Retornando a promise
			.fromFnPromise(function() {
				return getListaPaciente();
			})
			.withOption('destroy', true)
			.withPaginationType('full_numbers')
			.withDOM('<\'row\'<\'col-xs-6\'l><\'col-xs-6\'f>r>t<\'row\'<\'col-xs-6\'i><\'col-xs-6\'p>><\'row\'<\'col-xs-12\'T>>')
			.withColReorder()
			.withTableTools('bower_components/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf')
			.withTableToolsButtons([  
				'copy',
				'print', {
					'sExtends': 'collection',
					'sButtonText': 'Salvar',
					'aButtons': ['csv', 'xls', 'pdf']
				}
			])
			.withBootstrap()
			.withBootstrapOptions({
				TableTools: {
					classes: {
						container: 'btn-group',
						buttons: {
							normal: 'btn btn-default'
						}
					}
				},
				pagination: {
					classes: {
						ul: 'pagination pagination-sm'
					}
				}
			})
			.withOption('responsive', true)
			.withOption('createdRow', createdRow);
			
		vm.dtColumns = [
			DTColumnBuilder.newColumn('nmCli').withTitle('Nome Completo'),
			DTColumnBuilder.newColumn('nuIde').withTitle('RG').renderWith(function(data, type, full) {
				
				//Caso ocorra algum problema e o campo RG deste dado retornar vazio ou nulo, não irá atrapalhar a exibição de todos os registros.
				if(data != null && data.length > 7){
					//Coloca um ponto entre o terceiro e o quarto dígitos
					data=data.replace(/(\d{3})(\d)/,"$1.$2");
			 
					//Coloca um ponto entre o terceiro e o quarto dígitos
					//de novo (para o segundo bloco de números)
					data=data.replace(/(\d{3})(\d)/,"$1.$2");
			 
					//Coloca um hífen entre o terceiro e o quarto dígitos
					data=data.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
				}else{
					data = " ";
				};
				
				return data;
				
			}),
			DTColumnBuilder.newColumn('nuCel').withTitle('Celular').renderWith(function(data, type, full) {
				
				//Caso ocorra algum problema e o campo RG deste dado retornar vazio ou nulo, não irá atrapalhar a exibição de todos os registros.
				if(data != null && data.length > 7){
					data=data.replace(/(\d{0})(\d)/,"$1( $2");
					data=data.replace(/(\d{2})(\d)/,"$1 ) $2");
					data=data.replace(/(\d{5})(\d)/,"$1-$2");
				}else{
					data = " ";
				}; 
				
				return data;
				
			}),
			DTColumnBuilder.newColumn(null).withTitle('#').notSortable()
				.renderWith(actionsHtml)
		];

		function createdRow(row, data, dataIndex) {
			$compile(angular.element(row).contents())($scope);
		}
		
		function actionsHtml(data, type, full, meta) {
			return 	"<button class='btn btn-xs btn-warning' ng-click=vm.edit(" + "'" + data.id + "'" + ") >" +
					"   <i class='fa fa-edit'></i> " +
					"</button>&nbsp;" +
					"<button class='btn btn-xs btn-primary' ng-click=vm.gerenciar(" + "'" + data.id + "'" + ") >" +
					"   <i class='fa fa-wrench'></i>" +
					"</button>";
		};
		
		
		//Invocando função inicio.
		activate();

	}

})();

