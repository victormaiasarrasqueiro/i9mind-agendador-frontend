﻿(function() {
    'use strict';

    angular.module('app.paciente').controller('CadastrarAlterarClienteController', CadastrarAlterarClienteController);

	CadastrarAlterarClienteController.$inject = ['$scope', '$state', '$q', 'EnderecoService', 'ClienteService', 'DateUtilService', 'AlertService', 'ConvenioClienteService', 'cliente'];

    function CadastrarAlterarClienteController( $scope, $state, $q, EnderecoService, ClienteService, DateUtilService, AlertService, ConvenioClienteService, cliente ) {
        
		var vm = this;
		
		vm.inserirCliente = inserirCliente;
		vm.atualizarCliente = atualizarCliente;
		vm.exibirRemoverCliente = exibirRemoverCliente;
		vm.carregarCep = carregarCep;
		vm.exibirBuscaCep = exibirBuscaCep;
		
		//Interface
		vm.isAlterar = false;
		vm.nomeFunc  = "Cadastrar Paciente";
		vm.comp = "";
		
		//Inicializando objeto cliente.
		vm.cliente = {};
		vm.cliente.isRes = false;
		vm.cliente.isDef = false;
		vm.isBuscarCep   = false;
		vm.cliente.tpECi = "0";
		vm.cliente.tpSex = "0";
		vm.cliente.tpCon = "0";
		vm.cliente.idOsa = "0";
		vm.cliente.idRGp = "0";
		vm.cliente.idDfc = "0";
		vm.cliente.idOCe = "0";
		vm.cliente.idEcv = "0";
		vm.cliente.idOco = "0";
		vm.cliente.idOEU = "0";
		vm.cliente.idREU = "0";
		
		vm.listaUF = [];
		vm.listaConvOpeSau = [];
		vm.listaEmpConv = [];
		vm.listaOutConv = [];
		
		//Iniciando Google Address
		vm.address = {};
		vm.address.components = {};
		vm.address.place = {};

		
		//Função Inicio.
	  	function activate() {
			
			if(cliente != null){
					
				vm.isAlterar = true;
				vm.cliente = angular.copy(cliente);
				
				//Nome da Funcionalidade ( Titulo )
				vm.nomeFunc  = "Alterar Paciente";
				
				//Complemento ( subtitulo da página ) - Nome do Cliente.
				vm.comp = " - " + angular.copy(cliente.nmCli);

				//Formatando as datas de retorno.
				formatarDataRetorno();

			};
			
			var promises = [getListaUF(), getListaOperadoraSaude(), getListaEmpresaConveniada(), getListaOutrosConvenios()];
            
			return $q.all(promises);

        };

		/*  Inserir novo Cliente  */
		function inserirCliente(){
			
			tratarCamposCliente();

			return ClienteService.inserirCliente(vm.cliente).then(function(data, status, headers, config) {
				AlertService.sweetSucesso(1,"O paciente", redirecionarParaListagem);
				return data;
			});

		};
		
		/*  Atualizar Cliente  */
		function atualizarCliente(){
			
			tratarCamposCliente();
			
			return ClienteService.atualizarCliente(vm.cliente).then(function(data, status, headers, config) {
				AlertService.sweetSucesso(2,"O paciente", redirecionarParaListagem);
				return data;
			});

		};
		
		/*  Exibindo o alert de confirmação de exclusão  */
		function exibirRemoverCliente(){

			AlertService.sweetRemoverObj("este paciente", removerCliente);
			
		};
		
		/*  Excluir Cliente  */
		function removerCliente(){

			return ClienteService.removerCliente(vm.cliente.id).then(function(data, status, headers, config) {
				
				AlertService.sweetSucesso(3,"O paciente", redirecionarParaListagem);
				return data;
				
			});
			
		};

		// Tratando campos antes de incluir/alterar
		function tratarCamposCliente(){
			
			/* Tratando as datas da página*/
			vm.cliente.dtNas = DateUtilService.dateStrToIsoStr(vm.cliente.tdtNas);
			vm.cliente.dtOpV = DateUtilService.dateStrToIsoStr(vm.cliente.tdtOpV);
			vm.cliente.dtEcV = DateUtilService.dateStrToIsoStr(vm.cliente.tdtEcV);

		};
		
		// Formatando as datas vindas do banco, para o formato de tela ( mask )
		function formatarDataRetorno(){
		
			vm.cliente.tdtNas = DateUtilService.isoStrToFormatDateStr(vm.cliente.dtNas);
			vm.cliente.tdtOpV = DateUtilService.isoStrToFormatDateStr(vm.cliente.dtOpV);
			vm.cliente.tdtEcV = DateUtilService.isoStrToFormatDateStr(vm.cliente.dtEcV);
		
		};

		//Localizar o endereço pelo CEP.
		function carregarCep(){
			
			return EnderecoService.getEnderecoCep(vm.pesqCep).then(function(data, status, headers, config) {
				
				var endRet = data.data;
				vm.cliente.dsLog = endRet.logradouro;
				vm.cliente.dsBai = endRet.bairro;
				vm.cliente.dsCid = endRet.localidade;
				vm.cliente.dsEst = endRet.uf;
				vm.cliente.nuCep = endRet.cep;
				vm.cliente.dsPai = "Brasil";
				return data;
				
			});
		};
		
		//Localizar o endereço pelo Google.
		$scope.$watch('vm.address.place', function() {
			
			angular.forEach(vm.address.place.address_components, function(value, key) {

				switch(value.types[0]) {
					
					case "route":
						vm.cliente.dsLog = value.short_name;
						break;
					
					case "street_number":
						vm.cliente.nuLog = value.short_name;
						break;
						
					case "neighborhood":
						vm.cliente.dsBai = value.short_name;
						break;
						
					case "sublocality_level_1":
						vm.cliente.dsBai = value.short_name;
						break;
					
					case "locality":
					
						vm.cliente.dsLoc = value.short_name;
						break;
						
					case "administrative_area_level_2":
						vm.cliente.dsCid = value.short_name;
						break;
						
					case "administrative_area_level_1":
						vm.cliente.dsEst = value.short_name;
						break;
						
					case "country":
						vm.cliente.dsPai = value.long_name;
						break;
						
					case "postal_code":
						vm.cliente.nuCep = value.long_name;
						break;
				}

			});

		});
		
		vm.googleOptions = {
		  types: ['address'],
		  componentRestrictions: { country: 'BR' }
		};
		
		//Recuperar lista de UF
		function getListaUF(){
			return EnderecoService.getListaUF().then(function(data) {
				vm.listaUF = data;
				return data;
			});
		};
		
		//Recuperar lista de Operadora de Saude.
		function getListaOperadoraSaude(){
			return ConvenioClienteService.getListaOperadoraSaude().then(function(data) {
				vm.listaConvOpeSau = data;
				return data;
			});
		};
		
		//Recuperar lista de Empresas conveniadas.
		function getListaEmpresaConveniada(){
			return ConvenioClienteService.getListaEmpresaConveniada().then(function(data) {
				vm.listaEmpConv = data;
				return data;
			});
		};
			
		//Recuperar lista de Empresas conveniadas.
		function getListaOutrosConvenios(){
			return ConvenioClienteService.getListaOutrosConvenios().then(function(data) {
				vm.listaOutConv = data;
				return data;
			});
		};
		
		function exibirBuscaCep(){
			vm.exibirCep = ! vm.exibirCep;
			vm.shouldBeOpen = vm.exibirCep;
		};

		//Redirecionando para a página de listagem de clientes.
		function redirecionarParaListagem(){
			$state.transitionTo("app.cliente", {});
		};
		
		/*
		*
		*  Iniciando Controller
		*
		*/ 
		
		activate();
		

	};
	
})();

