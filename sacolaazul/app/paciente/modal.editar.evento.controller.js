﻿(function() {
    'use strict';

    angular.module('app.paciente').controller('ModalEditarEventoController', ModalEditarEventoController);

	ModalEditarEventoController.$inject = ['$log', '$http', '$scope', '$modalInstance', 'listaExcluir'];
	
    function ModalEditarEventoController( $log, $http, $scope, $modalInstance, listaExcluir ) {
        
		var vm = this; // vm = view-model
		
		vm.listaExcluir = listaExcluir;
		
		vm.listaProfissional = [
		
			{
				id:1,
				titulo:'Dr',
				nome: 'Paulo',
				sobrenome:'Fagundes Barbosa'
			},
			{
				id:2,
				titulo:'Dr',
				nome: 'Lucia',
				sobrenome:'Pinto'
			},
			{
				id:3,
				titulo:'Sr',
				nome: 'Jacarandira',
				sobrenome:'Moreira Noel Silva Pacheco Rosa'
			},
			{
				id:4,
				titulo:'Sra',
				nome: 'Ruth',
				sobrenome:'da Estrela da Silva Fagundes Paleta Fagundes'
			},
			{
				id:5,
				titulo:'Sr',
				nome: 'Hugo',
				sobrenome:'Baldessarine'
			},
			{
				id:6,
				titulo:'Dr',
				nome: 'Helio',
				sobrenome:'dos Anjos Moreira Albuquerque'
			}

		];
		
		vm.listaServico = [
		
			{
				id:1,
				nome: 'Consulta Clínica'
				
			},
			{
				id:2,
				nome: 'Exame de Vista'
			}

		];
		
		//Função responsável por inserir um novo evento no calendario.
		vm.excluir = function () {
			
			var novoEvento = {};
			novoEvento.id = 10;
			novoEvento.title = vm.nomeEvento + " - " + vm.nomePaciente;
			novoEvento.start = moment(vm.dataInicio).format();
			//novoEvento.start = "2015-09-07T09:50:00";
			//alert(vm.dataInicio);
			//alert
			
			novoEvento.end = moment(vm.dataFim);
			novoEvento.textColor = "white";
			novoEvento.backgroundColor = "pink";
			
			//Retornando o novo objeto criado.
			$modalInstance.close(novoEvento);
		};
		
		// Caso o usuario cancele a operacao.
		vm.cancelar = function () {
			$modalInstance.close();
		};

		//Função Inicializadora.
		vm.inicio = function () {

		};
		
		vm.inicio();
		
    }
	
		
})();

