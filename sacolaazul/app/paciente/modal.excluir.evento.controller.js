(function() {
    'use strict';

    angular.module('app.paciente').controller('ModalExcluirEventoController', ModalExcluirEventoController);

	ModalExcluirEventoController.$inject = ['$log', '$http', '$scope', '$modalInstance', 'listaExcluir'];
	
    function ModalExcluirEventoController( $log, $http, $scope, $modalInstance, listaExcluir ) {
        
		var vm = this; // vm = view-model
	
		vm.listaExcluir = angular.copy(listaExcluir);
		
		
		// Retirando o evento da lista
		function apagarEvento(evento) {
			for(var i = 0; i < vm.listaExcluir.length; i++){
				if(evento.id == vm.listaExcluir[i].id){
					 vm.listaExcluir.splice(i,1);
					 break;
				}
			}
		};
		
		// Caso o usuario cancele a operacao.
		vm.removerItemListaExcluir = function (id) {
			apagarEvento(vm.listaExcluir[id]);
		};
		
		//Func��o respons�vel por inserir um novo evento no calendario.
		vm.excluir = function () {
			
			var novoEvento = {};
			novoEvento.id = 10;
			novoEvento.title = vm.nomeEvento + " - " + vm.nomePaciente;
			novoEvento.start = moment(vm.dataInicio).format();
			//novoEvento.start = "2015-09-07T09:50:00";
			//alert(vm.dataInicio);
			//alert
			
			novoEvento.end = moment(vm.dataFim);
			novoEvento.textColor = "white";
			novoEvento.backgroundColor = "pink";
			
			//Retornando o novo objeto criado.
			$modalInstance.close(novoEvento);
		};
		
		// Caso o usuario cancele a operacao.
		vm.cancelar = function () {
			$modalInstance.close();
		};

		//Fun��o Inicializadora.
		vm.inicio = function () {

		};
		
		vm.inicio();
		
    }
	
		
})();

