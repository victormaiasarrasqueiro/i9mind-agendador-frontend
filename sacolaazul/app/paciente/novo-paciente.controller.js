﻿(function() {
    'use strict';

    angular.module('app.paciente').controller('CadastrarAlterarClienteController', CadastrarAlterarClienteController);

	CadastrarAlterarClienteController.$inject = ['$scope', '$state', 'EnderecoService', 'ClienteService', 'cliente'];

    function CadastrarAlterarClienteController( $scope, $state, EnderecoService, ClienteService, cliente ) {
        
		var vm = this;
		
		vm.inserirCliente = inserirCliente;
		vm.carregarCep = carregarCep;
		
		//Indicativo tela alterar
		vm.isAlterar = false;
		vm.nomeFunc  = "Cadastrar Paciente";
		
		//Inicializando objeto cliente.
		vm.cliente = {};
		vm.cliente.isRes = false;
		vm.cliente.isDef = false;
		vm.isBuscarCep   = false;
		vm.cliente.tpECi = "0";
		vm.cliente.tpSex = "0";
		vm.cliente.tpCon = "0";
		vm.cliente.idOsa = "0";
		vm.cliente.idRGp = "0";
		vm.cliente.idDfc = "0";
		vm.cliente.idOCe = "0";
		vm.cliente.idEcv = "0";
		vm.cliente.idTsc = "0";
		
		//Iniciando Google Address
		vm.address = {};
		vm.address.components = {};
		vm.address.place = {};

		
		//Função Inicio.
	  	function activate() {
			if(cliente != null){
			    vm.nomeFunc  = "Alterar Paciente - " + cliente.nmCli;
				vm.isAlterar = true;
				vm.cliente = angular.copy(cliente);
			};

        };
		
		/*  Inserir novo Cliente  */
		function inserirCliente(){
			if(validarCamposCliente()){
				tratarCamposCliente();
				return ClienteService.inserirCliente(vm.cliente).then(function(data, status, headers, config) {
					exibirAlertSucesso();
					return data;
				});
			}
		};
		
		// Validando campos antes de incluir/alterar
		function validarCamposCliente(){
			if(vm.cliente.nmCli == null || vm.cliente.nmCli.length < 4){
				alert("O nome do cliente precisa possuir mais de 3 letras.");
				return false;
			};
			if(vm.cliente.nuCel == null || vm.cliente.nuCel.length < 11){
				alert("Data de nascimento preenchida incorretamente.");
				return false;
			};
			return true;
		};
		
		// Tratando campos antes de incluir/alterar
		function tratarCamposCliente(){
			
			/* Tratando as datas da página*/
			if(vm.cliente.tdtNas != null && vm.cliente.tdtNas.length == 8){
				vm.cliente.dtNas = moment(vm.cliente.tdtNas, "DDMMYYYY").format();
			}else{
				vm.cliente.dtNas = null;
			};
			
			if(vm.cliente.tdtOpV != null && vm.cliente.tdtOpV.length == 8){
				vm.cliente.dtOpV = moment(vm.cliente.tdtOpV, "DDMMYYYY").format();
			}else{
				vm.cliente.dtOpV = null;
			};
			
			if(vm.cliente.tdtEcV != null && vm.cliente.tdtEcV.length == 8){
				vm.cliente.dtEcV = moment(vm.cliente.tdtEcV, "DDMMYYYY").format();
			}else{
				vm.cliente.dtEcV = null;
			};

		};

		function exibirAlertSucesso(){
			
			swal
			(
				{
					title: "Sucesso!", 
					text: "O novo paciente foi gravado com sucesso.", 
					type: "success",
					showCancelButton: false,
					closeOnConfirm: true,
					confirmButtonText: "Ok",
					confirmButtonColor: "#70bafd"
				}, 
				function() 
				{
					//Redirecionando para a página de listagem.
					$state.transitionTo("app.paciente", { id: "123" });		
				}
			);
			
		};
		
		//Localizar o endereço pelo CEP.
		function carregarCep(){
			
			return EnderecoService.getEnderecoCep(vm.pesqCep).then(function(data, status, headers, config) {
				
				var endRet = data.data;
				vm.cliente.dsLog = endRet.logradouro;
				vm.cliente.dsBai = endRet.bairro;
				vm.cliente.dsCid = endRet.localidade;
				vm.cliente.dsEst = endRet.uf;
				vm.cliente.nuCep = endRet.cep;
				vm.cliente.dsPai = "Brasil";
				return data;
				
			});
		};
		
		//Localizar o endereço pelo Google.
		$scope.$watch('vm.address.place', function() {
			
			angular.forEach(vm.address.place.address_components, function(value, key) {

				switch(value.types[0]) {
					
					case "route":
						vm.cliente.dsLog = value.short_name;
						break;
					
					case "street_number":
						vm.cliente.nuLog = value.short_name;
						break;
						
					case "neighborhood":
						vm.cliente.dsBai = value.short_name;
						break;
						
					case "sublocality_level_1":
						vm.cliente.dsBai = value.short_name;
						break;
					
					case "locality":
					
						vm.cliente.dsLoc = value.short_name;
						break;
						
					case "administrative_area_level_2":
						vm.cliente.dsCid = value.short_name;
						break;
						
					case "administrative_area_level_1":
						vm.cliente.dsEst = value.short_name;
						break;
						
					case "country":
						vm.cliente.dsPai = value.long_name;
						break;
						
					case "postal_code":
						vm.cliente.nuCep = value.long_name;
						break;
				}

			});

		});
		
		vm.googleOptions = {
		  types: ['address'],
		  componentRestrictions: { country: 'BR' }
		};

		/*
		*
		*  Iniciando Controller
		*
		*/ 
		
		activate();
		

	};
	
})();

