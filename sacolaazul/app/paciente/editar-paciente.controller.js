﻿(function() {
    'use strict';

    angular.module('app.paciente').controller('EditarPacienteController', EditarPacienteController);

	EditarPacienteController.$inject = ['$log', '$http', '$scope','$timeout', '$modal' , 'EventoService', 'CalendarioService', '$q'];

    function EditarPacienteController( $log, $http, $scope, $timeout, $modal, EventoService, CalendarioService, $q) {
        
		var vm = this; // vm = view-model
		
		/* OBJETO PACIENTE  */
		vm.paciente = {};
		vm.listaPacienteSelecionado = [];   //Pacientes selecionados na aba agenda   
		vm.listaEventoPacienteProximo = []; //Lista dos proximos eventos que o paciente esta agendado.   listaEventoPacienteProximo
		vm.listaEventoPacienteAnterior = []; //Lista dos eventos anteriores que o paciente esta agendado.
		
		/* CALENDARIO - CONFIGURACAO  */
		vm.uiConfig = {}; 
		vm.uiConfig.calendar = {}; 
		vm.eventSources = [];
		vm.configUserCalendar = {};
		
		/* CARREGAMENTO DE INFORMACOES DAS ABAS  */
		vm.carregarAbaAgenda = carregarAbaAgenda;
		
		/* EVENTOS DE PAGINA  */
		vm.mostrarDataCalendarioProximo  = mostrarDataCalendarioProximo;
		vm.mostrarDataCalendarioAnterior = mostrarDataCalendarioAnterior;
		vm.excluirListaEventoSelecionado = excluirListaEventoSelecionado;
		vm.editarListaEventoSelecionado = editarListaEventoSelecionado;
		vm.marcarTodos = marcarTodos;
		
		vm.listaProximoHorarioDisponivel = [];
		
		activate();
		
		function activate() {

			//Carregando as configurações Basicas do Calendario;
			vm.uiConfig.calendar = CalendarioService.getDefaultCalendar(); 
			
			//Configurando os eventos para o calendario.
			montarEventosCalendario();
			
			var promises = [getListaEventoPacienteProximo(), getListaEventoPacienteAnterior(), carregarConfigUserCalendar()];
            return $q.all(promises).then(function(){
				
				vm.eventSources.push(vm.listaEventoPacienteProximo);
				vm.eventSources.push(vm.listaEventoPacienteAnterior);

			});
			
        };

		/* Retornando uma lista dos proximos eventos do paciente. */
		function getListaEventoPacienteProximo() {

			return EventoService.getListaEventoPacienteProximo()
				.then(function(data){
					vm.listaEventoPacienteProximo = data.data;
					return data.data;
			});
				
        };
		
		/* Retornando uma lista dos eventos anteriores do paciente. */
		function getListaEventoPacienteAnterior() {

			return EventoService.getListaEventoPacienteAnterior()
				.then(function(data){
					vm.listaEventoPacienteAnterior = data.data;
					return data.data;
				});
				
        };
		
		/* Carregando as configurações do calendario personalizadas para o usuario logado. */
		function carregarConfigUserCalendar() {

			return CalendarioService.carregarConfigUserCalendar()
				.then(function(data){
					vm.configUserCalendar = data.data;
					montarCalendarioPersonalizadoUsuario();
					return data.data;
				});
				
        };
		
		/* Monta o calendario com as propriedades personalizadas do usuário.  */
		function montarCalendarioPersonalizadoUsuario(){
			
			// Informações personalizadas do usuário.
			vm.uiConfig.calendar.defaultView = vm.configUserCalendar.defaultView;
			vm.uiConfig.calendar.weekends = vm.configUserCalendar.weekends;
			vm.uiConfig.calendar.minTime = vm.configUserCalendar.minTime;
			vm.uiConfig.calendar.maxTime = vm.configUserCalendar.maxTime;
			vm.uiConfig.calendar.defaultTimedEventDuration = vm.configUserCalendar.defaultTimedEventDuration;
			vm.uiConfig.calendar.slotDuration = vm.configUserCalendar.slotDuration;
			vm.uiConfig.calendar.snapDuration = vm.configUserCalendar.snapDuration;
			vm.uiConfig.calendar.businessHours = vm.configUserCalendar.businessHours;

		};

		/* Carregando aba Agenda  */
		function carregarAbaAgenda(){

			renderCalender();
			
		};
		
		/* Renderizando o Calendario dentro do TabSet - Aba Agenda */
		function renderCalender() {
		   
		   $timeout(function(){
				
				$('#calendarAgendaPaciente').fullCalendar('render');
				$('#calendarAgendaPaciente').fullCalendar('rerenderEvents');
				
			}, 0);
			
		};

		/* Mostrando a data clicada dentro do Calendario  */
		function mostrarDataCalendarioProximo(index) {		  

			//Definindo valor do checkbox e objeto ao clicar na linha.
			if(angular.isUndefined(vm.listaEventoPacienteProximo[index].selecionado)){
				vm.listaEventoPacienteProximo[index].selecionado = true;
			}else{
				vm.listaEventoPacienteProximo[index].selecionado = !vm.listaEventoPacienteProximo[index].selecionado;
			}

			//Setando a data no Calendario.
			if(vm.listaEventoPacienteProximo[index].selecionado){
				$('#calendarAgendaPaciente').fullCalendar('gotoDate',vm.listaEventoPacienteProximo[index].start);
				vm.listaEventoPacienteProximo[index].backgroundColor = "#00C6FF";
				vm.listaEventoPacienteProximo[index].textColor = "black";
				$('#calendarAgendaPaciente').fullCalendar('render');
				$('#calendarAgendaPaciente').fullCalendar('rerenderEvents');
				incluirEvento(vm.listaEventoPacienteProximo[index]);
				
			}else{
				vm.listaEventoPacienteProximo[index].backgroundColor = "blue";
				vm.listaEventoPacienteProximo[index].textColor = "white";
				$('#calendarAgendaPaciente').fullCalendar('render');
				$('#calendarAgendaPaciente').fullCalendar('rerenderEvents');
				apagarEvento(vm.listaEventoPacienteProximo[index]);
			}
			
		};
		
		/* Mostrando a data clicada dentro do Calendario  */
		function mostrarDataCalendarioAnterior(index) {		  

			$('#calendarAgendaPaciente').fullCalendar('gotoDate',vm.listaEventoPacienteAnterior[index].start);
			$('#calendarAgendaPaciente').fullCalendar('render');
			$('#calendarAgendaPaciente').fullCalendar('rerenderEvents');

		};
		
				
		/* Marcando todos os checkbox da lista para 'true' */
		function marcarTodos() {	
			
			vm.listaPacienteSelecionado = [];
			
			for(var i = 0; i < vm.listaEventoPacienteProximo.length; i++){
				
				vm.listaEventoPacienteProximo[i].selecionado = vm.checkBoxMarcarTodos;
				
				if(vm.checkBoxMarcarTodos){
					vm.listaPacienteSelecionado.push(vm.listaEventoPacienteProximo[i]);			
				}
				
			}

		};
		
		//Incluir o evento passado.
		function incluirEvento(evento) {
			
			var existe = false;
			
			for(var i = 0; i < vm.listaPacienteSelecionado.length; i++){
				
				if(evento.id == vm.listaPacienteSelecionado[i].id){
					 existe = true;
					 break;
				}
				
			}
			
			if(!existe){
				vm.listaPacienteSelecionado.push(evento);
			}
			
		};
		
		//Apagando o evento passado.
		function apagarEvento(evento) {
		
			for(var i = 0; i < vm.listaPacienteSelecionado.length; i++){
				
				if(evento.id == vm.listaPacienteSelecionado[i].id){
					 vm.listaPacienteSelecionado.splice(i,1);
					 break;
				}
				
			}
			
		};

		/* Exibindo o modal de exclusao de Eventos selecionados na aba agenda. */
		function excluirListaEventoSelecionado() {		  

			var modalInstance = $modal.open({
				animation:true,
				size:"lg",
				templateUrl: 'app/paciente/modal.excluir.evento.html',
				controller: 'ModalExcluirEventoController',
				controllerAs: 'vm',
				bindToController:true,
				resolve: {
				
					listaExcluir: function () {
					  return vm.listaPacienteSelecionado;
					}
					
				}
			});
			modalInstance.result.then(
			
				function () {
					//Nenhum Selecionado.
				}
				
			);
			
		};
		
		/* Exibindo o modal de edição de Eventos selecionados na aba agenda. */
		function editarListaEventoSelecionado() {		  

			var modalInstance = $modal.open({
				animation:true,
				size:"lg",
				templateUrl: 'app/paciente/modal.editar.evento.html',
				controller: 'ModalEditarEventoController',
				controllerAs: 'vm',
				bindToController:true,
				resolve: {
				
					listaExcluir: function () {
					  return vm.listaPacienteSelecionado;
					}
					
				}
			});
			modalInstance.result.then(
			
				function () {
					//Nenhum Selecionado.
				}
				
			);
			
		};
		
		/* Monta o calendario com as propriedades personalizadas do usuário.  */
		function montarEventosCalendario(){

			// Eventos
			vm.uiConfig.calendar.select = function(start,end){inserirEvento(start,end);};
			vm.uiConfig.calendar.eventResize = function(event, delta, revertFunc, jsEvent, ui, view ){
				var ask = confirm("Deseja realmente remarcar este evento?");
				if(ask){
					// Atualizar novo horário.
				}else{
					revertFunc();
				}
			};
			vm.uiConfig.calendar.eventDrop = function( event, delta, revertFunc, jsEvent, ui, view ){
				var ask = confirm("Deseja realmente remarcar este evento?");
				if(ask){
					// Atualizar novo horário.
				}else{
					revertFunc();
				}
			};
			vm.uiConfig.calendar.eventClick = function(event){
				//editarEventoDiaClicado(event);
			};
			
		};

		function inserirEvento(start,end) {

			var modalInstance = $modal.open({
				animation:true,
				size:"lg",
				templateUrl: 'app/modalagenda/modal.agenda.novoevento.html',
				controller: 'ModalAgendaNovoEventoController',
				controllerAs: 'vm',
				bindToController:true,
				resolve: {
					usuario: function () {
					  return "10";
					},
					dataInicio: function () {
					  return start;
					},
					dataFim: function () {
					  return end;
					}
				}
			});
			modalInstance.result.then(function (novoEvento) {
			  vm.listaEventoPacienteProximo.push(novoEvento); 
			}, function () {
				//Nenhum Selecionado.
			});
		};
				
		function editarEventoDiaClicado(date) {
			var modalInstance = $modal.open({
				animation:true,
				size:"lg",
				templateUrl: 'app/modalagenda/modal.agenda.novoevento.html',
				controller: 'ModalAgendaNovoEventoController',
				controllerAs: 'vm',
				bindToController:true,
				resolve: {
					dataSelecionada: function () {
					  return date;
					}
				}
			});
			modalInstance.result.then(function (novoEvento) {
			  novosEventos.push(novoEvento);
			}, function () {
			  alert("SEM PARAMETRO");
			});
		};

		
		
		
		
		//Busca Data
		
		
		
		function getMomentDataInicio(){
			var data = moment({ hour:08, minute:0 });
			return data;
		}
		
		function getMomentDataFim(){
			var data = moment({ hour:17, minute:0 });
			return data;
		}
		
		
		vm.dataInicio = getMomentDataInicio();
		vm.dataFim = getMomentDataFim();
		
		
		/*
		* **********************************************************************************************************************************
		* Verificando se a Data Corrente informada, com o determinado período, se localiza dentro do range de perídodos informados na tela.
		* **********************************************************************************************************************************
		*/
		function verificarRangeDataFiltro(data,duracao){
		
			//Selecionando o Range que estamos consultando ( Para saber se pode ter um evento nesse range de tempo ou nao.
			
			var endDuracaoDataCorrente = angular.copy(data);
			endDuracaoDataCorrente.add(duracao, "m");

			
			//Verificando se o dia selecionado esta dentro do horário determinado.
			//Criando um range de horários permitidos para o dia corrente.
			
			// 1- Conveterno o objeto data to moment
			var momentHoraInicioFiltro = moment(vm.dataInicio);
			var momentHoraFimFiltro = moment(vm.dataFim);
			
			// 2- Copiando objeto Data Corrente e criando um range de horarios.

			var dataInicio = angular.copy(data);
			dataInicio.set('hour', momentHoraInicioFiltro.hour());
			dataInicio.set('minute',momentHoraInicioFiltro.minute());
			
			var dataFim = angular.copy(data);
			dataFim.set('hour', momentHoraFimFiltro.hour());
			dataFim.set('minute', momentHoraFimFiltro.minute());
			
			
			//Criando o range para sabermos se o range corrente esta dentro do range indicado no filtro da página.
			var rangeHorarioSelecionadoFiltro = moment.range(dataInicio,dataFim);

			
			//Se a data corrente sobrepoe a area permitida Retorna OK.
			if(! (data.within(rangeHorarioSelecionadoFiltro) && endDuracaoDataCorrente.within(rangeHorarioSelecionadoFiltro)) ){

				return true;
			};
		
			return false;
			
		};
		
		/*
		* **********************************************************************************************************************************
		* Verificando se a data/hora corrente esta dentro do dia informado no filtro.
		* **********************************************************************************************************************************
		*/
		function verificandoDiaSemanaFiltro(data){
		
			//Verificando se o dia corrente é um dia selecionado no filtro.
			if(vm.diaSemanaSelecionada != -1  && data.day() != vm.diaSemanaSelecionada){
				return true;
			};
			
			return false;
		};
		
		/*
		* **********************************************************************************************************************************
		* Verificando se a data/hora corrente esta indo de encontro de algum evento já marcado.
		* **********************************************************************************************************************************
		*/
		function verificandoDataHoraReservadaOutroEvento(data,duracao){
			
			var endDuracaoDataCorrente = angular.copy(data);
			endDuracaoDataCorrente.add(duracao, "m");
			
			var dataCorrenteRange = moment.range( angular.copy(data), endDuracaoDataCorrente );
			
			//Lista Proximos eventos ( Depois deverá ser substituido pela lista de proximas consultas do medico ( independente do paciente )
			var listaProximaConsulta = vm.listaEventoPacienteProximo;

			for(var i = 0; i < listaProximaConsulta.length ; i++){

				var rangeEventoCadastrado = moment.range(moment(listaProximaConsulta[i].start), moment(listaProximaConsulta[i].end));
				if(dataCorrenteRange.overlaps(rangeEventoCadastrado)){
					return true;
				};
				
			};
			
		};
		
		
		function isDataReservada(data,duracao){
			
			// Verificando se a data/hora corrente esta dentro do dia informado no filtro.
			if(verificandoDiaSemanaFiltro(data)){
				return true;
			};
			
			// Verificando se a data/hora corrente esta dentro do range de horários informados no filtro.
			if(verificarRangeDataFiltro(data,duracao)){
				return true;
			};
			
			// Verificando se a data/hora corrente esta indo de encontro de algum evento já marcado.
			if(verificandoDataHoraReservadaOutroEvento(data,duracao)){
				return true;
			};

			return false;

		};

		vm.calcularListaProximoHorarioDisponivel = calcularListaProximoHorarioDisponivel;
		
		function calcularListaProximoHorarioDisponivel(){
			/*
			alert("COMECA4");
			
			var data1  = moment("2015-01-01 15:00", "YYYY-MM-DD HH:mm");
			var data2  = moment("2015-01-01 18:00", "YYYY-MM-DD HH:mm");
			
			var data3  = moment("2015-01-01 17:50", "YYYY-MM-DD HH:mm");
			var data4  = moment("2015-01-01 20:00", "YYYY-MM-DD HH:mm");
			
			alert(data1);
			alert(data2);
			
			var range1 = moment.range(data1,data2);
			var range2 = moment.range(data3,data4);
			
			
			alert(range1);
			alert(range2);
			
			alert(range2.overlaps(range1));
			
			*/

			vm.listaProximoHorarioDisponivel = [];
			
			//ESTA LOGICA DE PROGRAMACAO DEVERA IR PARA O NODEJS

			var intervaloEvento = 10; // Recuperando o intervalo.
			
			//Definindo Agora
			var momentNow =  moment();
			momentNow.seconds(0);
			momentNow.minutes(0);
			momentNow.add(60, "m");
			momentNow.month(9);
			momentNow.date(4);
			
			momentNow.hour(12);
		
			
			//Definindo o numero de horarios vagos que serão retornados.
			var numMaxRetorno = 20;
			
			//Definindo o tempo máximo que será pesquisado
			var numTempoMaximoPesquisa = 60;
			
			//Defindo a data máxima no qual será retornado os eventos marcados.
			var tempoMaximoRetorno = angular.copy(momentNow);
			tempoMaximoRetorno.add(numTempoMaximoPesquisa, "d");

			//Adcionando o proximo horario a ser pesquisado.
			var tempoCorrente = angular.copy(momentNow);
			tempoCorrente.add(intervaloEvento, "m");
			
						
		
			
			var duracao = 50;
			
			while(tempoCorrente.isBefore(tempoMaximoRetorno) && vm.listaProximoHorarioDisponivel.length < 36){
				
			

				if(!isDataReservada(angular.copy(tempoCorrente),duracao)){
				
					vm.listaProximoHorarioDisponivel.push(angular.copy(tempoCorrente));
				};
				
				tempoCorrente.add(intervaloEvento, "m");
			

			}

		};
		

		
		$scope.timeOptions = {
			readonlyInput: true,
			showMeridian: false
		};
		
		$scope.open = {
			dataInicio: false,
			dataFim: false
		};

		$scope.openCalendar = function(e, date) {
			
			e.preventDefault();
			e.stopPropagation();
			
			if(date == 'dataInicio'){
				$scope.open.dataFim = false;
			}else{
				$scope.open.dataInicio = false;
			}

			$scope.open[date] = true;
		}

	}

})();

