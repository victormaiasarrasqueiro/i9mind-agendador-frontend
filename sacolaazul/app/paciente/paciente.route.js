﻿(function() {

    'use strict';

    angular.module('app.paciente').run(appRun);

    appRun.$inject = ['routerHelper','ClienteService'];

    function appRun(routerHelper, ClienteService) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
		
		{
            state: 'app.cadastrarCliente',
            config: {
                url: '/paciente/cadastrar',
                title: 'Cadastrar Novo Paciente',
                views: {
                    'content@app': {
                        templateUrl: 'app/paciente/cadastrar-alterar-cliente.html',
                        controller: 'CadastrarAlterarClienteController',
                        controllerAs: 'vm'
                    }
                },
				resolve:{

					cliente: function(){

						return null;
					}
					
				},
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        },
		{
            state: 'app.alterarCliente',
            config: {
                url: '/paciente/alterar/:id',
                title: 'Alterar Paciente',
                views: {
                    'content@app': {
                        templateUrl: 'app/paciente/cadastrar-alterar-cliente.html',
                        controller: 'CadastrarAlterarClienteController',
                        controllerAs: 'vm'
                    }
                },
				resolve:{

					cliente: function($stateParams,ClienteService){

						return ClienteService.obterCliente($stateParams.id).then(function(data){ return data; });
					}
					
				},
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        },
		{
            state: 'app.gerenciarCliente',
            config: {
                url: '/paciente/gerenciar/:id',
                title: 'Gerênciar Paciente',
                views: {
                    'content@app': {
                        templateUrl: 'app/paciente/gerenciar-cliente.html',
                        controller: 'GerenciarClienteController',
                        controllerAs: 'vm'
                    }
                },
				resolve:{

					cliente: function($stateParams,ClienteService){

						return ClienteService.obterCliente($stateParams.id).then(function(data){ return data; });
					}
					
				},
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        },
		{
            state: 'app.cliente',
            config: {
                url: '/paciente',
                title: 'Pacientes',
                views: {
                    'content@app': {
                        templateUrl: 'app/paciente/paciente.html',
                        controller: 'PacienteController',
                        controllerAs: 'vm'
                    }
                },
                data: {
                    permissions: {
                        only: ['administrator']
                    }
                }
            }
        }];
    }

})();
