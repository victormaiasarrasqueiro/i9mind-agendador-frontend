(function() {
    'use strict';

    angular.module('app', [
        'app.core',
		'app.atividade',
		'app.paciente',
		'app.produto',
		'app.cadastrobase',
		'app.profissional'
    ]);

})();