(function() {
    'use strict';

    angular.module('modal.foto').controller('ModalFotoController', ModalFotoController);

	ModalFotoController.$inject = ['$log', '$http', '$scope', '$modalInstance', 'foto'];
	
    function ModalFotoController( $log, $http, $scope, $modalInstance, foto ) {
        
		var vm = this; // vm = view-model
		
		vm.cancelar = cancelar;
		vm.inserir = inserir;
		
		vm.cropper = {};
        vm.cropper.sourceImage = null;
        vm.cropper.croppedImage   = null;
        vm.bounds = {};
        vm.bounds.left = 307;
        vm.bounds.right = 614;
        vm.bounds.top = 563;
        vm.bounds.bottom = 225;
		
		// Caso o usuario cancele a operacao.
		function cancelar() {
			$modalInstance.close();
		};
		
		//Func��o respons�vel por inserir a foto no formulario que o chamou.
		function inserir () {
			//Retornando o novo objeto criado.
			$modalInstance.close(angular.copy(vm.cropper.croppedImage));
		};
		
		//Fun��o Inicializadora.
		vm.inicio = function () {
			
			vm.cropper.croppedImage = foto;
			
		};
		
		vm.inicio();
		
    }
	
		
})();

