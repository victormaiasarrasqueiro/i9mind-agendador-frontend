﻿/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.produto').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.servico',
				config: {
					url: '/servicos',
					title: 'Servicos',
					views: {
						'content@app': {
							templateUrl: 'app/produto/servico.html',
							controller: 'ServicoController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
			},
			{
				state: 'app.cadastrarServico',
				config: {
					url: '/servicos/cadastrar',
					title: 'Servicos',
					views: {
						'content@app': {
							templateUrl: 'app/produto/cadastrar-alterar-servico.html',
							controller: 'CadastrarAlterarServicoController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
			}
		
		];
    };

})();
