﻿(function() {
    'use strict';

    angular.module('app.paciente').controller('CadastrarAlterarServicoController', CadastrarAlterarServicoController);

	CadastrarAlterarServicoController.$inject = ['$scope', '$state', '$q', 'EnderecoService', 'ClienteService', 'DateUtilService', 'AlertService', 'ConvenioClienteService'];

    function CadastrarAlterarServicoController( $scope, $state, $q, EnderecoService, ClienteService, DateUtilService, AlertService, ConvenioClienteService ) {
        
		var vm = this;
		
		vm.inserirServico = inserirServico;
		vm.atualizarServico = atualizarServico;
		vm.exibirRemoverServico = exibirRemoverServico;

		//Interface
		vm.isAlterar = false;
		vm.nomeFunc  = "Cadastrar Serviço";
		vm.comp = "";
		
 $scope.itemArray = [
        {id: 1, name: 'first'},
        {id: 2, name: 'second'},
        {id: 3, name: 'third'},
        {id: 4, name: 'fourth'},
        {id: 5, name: 'fifth'},
    ];

    $scope.selectedItem= $scope.itemArray[0];
	
		vm.listaProdutos = [{"nmPro":"Papel Higiênico", "id":"1","vrPro":"5.50"},{"nmPro":"Copo Plastico","id":"2","vrPro":"15.50"},{"nmPro":"Detergente","id":"3","vrPro":"3.50"}];
		
		//Inicializando objeto cliente.
		vm.sevico = {};
		
		//Função Inicio.
	  	function activate() {

			//var promises = [getListaUF(), getListaOperadoraSaude(), getListaEmpresaConveniada(), getListaOutrosConvenios()];
            
			//return $q.all(promises);

        };

		/*  Inserir novo Cliente  */
		function inserirServico(){
			
			tratarCamposCliente();

			return ClienteService.inserirCliente(vm.cliente).then(function(data, status, headers, config) {
				AlertService.sucesso(1,"O paciente", redirecionarParaListagem);
				return data;
			});

		};
		
		/*  Atualizar Cliente  */
		function atualizarServico(){
			
			tratarCamposCliente();
			
			return ClienteService.atualizarCliente(vm.cliente).then(function(data, status, headers, config) {
				AlertService.sucesso(2,"O paciente", redirecionarParaListagem);
				return data;
			});

		};
		
		/*  Exibindo o alert de confirmação de exclusão  */
		function exibirRemoverServico(){
			AlertService.romoverObj("este paciente", removerCliente);
		};
		
		/*  Excluir Cliente  */
		function removerCliente(){

			return ClienteService.removerCliente(vm.cliente.id).then(function(data, status, headers, config) {
				
				AlertService.sucesso(3,"O paciente", redirecionarParaListagem);
				return data;
				
			});
			
		};

		//Redirecionando para a página de listagem de clientes.
		function redirecionarParaListagem(){
			$state.transitionTo("app.cliente", {});
		};
		
		/*
		*
		*  Iniciando Controller
		*
		*/ 
		
		activate();
		

	};
	
})();

