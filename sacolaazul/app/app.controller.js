(function() {
  'use strict';

  angular.module('app').controller('AppController', AppController);

  AppController.$inject = ['$scope','DTDefaultOptions','DTOptionsBuilder'];

  function AppController($scope,DTDefaultOptions,DTOptionsBuilder) {
    
	var vm = this; // vm = view-model
    vm.appName = 'wdev-poc';
	
	$scope.tituloFuncionalidade = "Teste Victor Maia";
	
	DTDefaultOptions.setLanguage({
		sUrl: 'bower_components/datatables/lang/pt.json'
	});

  }

})();