﻿(function () {
    'use strict';

    angular.module('app').factory('EventoService', EventoService);

    EventoService.$inject = ['$http'];

    function EventoService($http) {
        
		var service = {
            getListaEventoPacienteProximo: getListaEventoPacienteProximo,
			getListaEventoPacienteAnterior: getListaEventoPacienteAnterior
        };

        return service;
		
		
		/**
		 * getListaEventoPacienteProximo()
		 *
		 * Retorna uma lista dos proximos eventos agendados para o paciente.
		 * @param  paciente - paciente no qual sera realizado a busca.
		 * @return lista dos proximos eventos agendados para o paciente.
		 */
        function getListaEventoPacienteProximo(paciente) {

			return $http({method: 'GET', url: 'evento-proximo.json'}).
                success(function(data, status, headers, config) {
					return data;
                }).
                error(function(data, status, headers, config) {
					alert(headers);alert(config);
                });
				
        };

		/**
		 * getListaEventoPacienteAnterior()
		 *
		 * Retorna uma lista dos eventos anteriores agendados para o paciente.
		 * @param  paciente - paciente no qual sera realizado a busca.
		 * @return lista dos proximos eventos agendados para o paciente.
		 */
		function getListaEventoPacienteAnterior(paciente) {

			return $http({method: 'GET', url: 'evento-anterior.json'}).
                success(function(data, status, headers, config) {
					
					return data;
                }).
                error(function(data, status, headers, config) {

                });
				
        };
		
    };
	
})();