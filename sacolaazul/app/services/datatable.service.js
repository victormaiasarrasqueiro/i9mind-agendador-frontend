﻿(function () {
    'use strict';

    angular.module('app').factory('DataTableService', DataTableService);

    DataTableService.$inject = ['DTDefaultOptions', 'DTOptionsBuilder', 'DTColumnBuilder'];

    function DataTableService(DTDefaultOptions, DTOptionsBuilder, DTColumnBuilder) {
        
		var service = {
            getDefaultDTOptionsBuilder: getDefaultDTOptionsBuilder
        };

        return service;

        function getDefaultDTOptionsBuilder(success) {
			
			DTDefaultOptions.setLanguage({
				sUrl: 'bower_components/datatables/lang/pt.json'
			});
			
			alert(DTOptionsBuilder);

			success( DT.withPaginationType('full_numbers')
			.withDOM('<\'row\'<\'col-xs-6\'l><\'col-xs-6\'f>r>t<\'row\'<\'col-xs-6\'i><\'col-xs-6\'p>><\'row\'<\'col-xs-12\'T>>')
			.withColReorder()
			.withColReorderOrder([1, 0, 2])
			.withTableTools('bower_components/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf')
			.withTableToolsButtons([  
				'copy',
				'print', {
					'sExtends': 'collection',
					'sButtonText': 'Salvar',
					'aButtons': ['csv', 'xls', 'pdf']
				}
			])
			.withBootstrap()
			.withBootstrapOptions({
				TableTools: {
					classes: {
						container: 'btn-group',
						buttons: {
							normal: 'btn btn-default'
						}
					}
				},
				pagination: {
					classes: {
						ul: 'pagination pagination-sm'
					}
				}
			})
			.withOption('responsive', true)
			.withOption('createdRow', createdRow));
			
		
			
        }
		
    }
	
})();