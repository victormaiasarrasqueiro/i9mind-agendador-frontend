﻿(function () {
    'use strict';

    angular.module('app').factory('DateUtilService', DateUtilService);

    DateUtilService.$inject = ['$q'];

    function DateUtilService($q) {
		
		var week = [{'idDay':'0','dsDay':'Domingo'},{'idDay':'1','dsDay':'Segunda-feira'},{'idDay':'2','dsDay':'Terça-feira'},{'idDay':'3','dsDay':'Quarta-feira'},{'idDay':'4','dsDay':'Quinta-feira'},{'idDay':'5','dsDay':'Sexta-feira'},{'idDay':'6','dsDay':'Sábado'}];
												  
		var service = {
			getWeek:getWeek,
			getJavaScriptDateFromMoment: getJavaScriptDateFromMoment,
			isoStrToFormatDateStr:isoStrToFormatDateStr,
			dateStrToIsoStr:dateStrToIsoStr,
			getAge:getAge,
			isHoraEndMenorStart:isHoraEndMenorStart,
			isPeriodoSobrepondoOutros:isPeriodoSobrepondoOutros
        };

        return service;
		
		/*
		* getWeek
		* Obter uma lista de dias da semana cadastrados.
		*/
        function getWeek() {

			var deferred = $q.defer();

			deferred.resolve(week);	
		
			return deferred.promise;

        };
		
		/**
		 * getJavaScriptDateFromMoment
		 *
		 * Retorna um objeto Date Java Script Puro apartir de um Wrapper Moment.
		 * @param  data - data
		 * @return date
		 */
		function getJavaScriptDateFromMoment(data) {

			var anoSelecionado    = data.year();
			var mesSelecionado    = data.month();
			var diaSelecionado    = data.date();
			var horaSelecionado   = data.hour();
			var minutoSelecionado = data.minute();
			
			return new Date(anoSelecionado, mesSelecionado, diaSelecionado, horaSelecionado, minutoSelecionado, 0, 0);
		
		};
		
		/**
		 * isoStrToFormatDateStr
		 *
		 * Passado uma STRING ISO (Padrao e definido como retorno do JAVA ), retorna uma STRING no formato desejado. PADRAO RETORNO DDMMYYYY
		 * @param  data - data, f FORMAT
		 * @return String Date - Default: DDMMYYYY
		 */
		function isoStrToFormatDateStr(date,f){
			
			if(date != null && date.length > 8){
			
				var format = "DDMMYYYY"
				var momentDate = moment(date);
				
				if(momentDate.isValid()){
					
					if(f!=null && f.lenght > 0){
						format = f;
					}
				
					return momentDate.format(format); 
				
				}else{
					
					return ""
					
				};
				
			}else{
			
				return ""
			
			}

		};
		
		/**
		 * dateStrToIsoStr
		 *
		 * Passado uma STRING DDMMYYYY retorna uma STRING no formato ISO
		 * @param  data - data, f FORMAT
		 * @return String Date ISO
		 */
		function dateStrToIsoStr(date){
			
			if(date != null && date.length == 8){
				
				return moment(date, "DDMMYYYY").format();

			}else{
			
				return null;
				
			};
			
		};

		/**
		 * isoStrToJsDate
		 *
		 * Passado uma STRING DDMMYYYY retorna uma STRING no formato ISO
		 * @param  data - data, f FORMAT
		 * @return String Date ISO
		 */
		function getAge(date){

			var age = moment(date);
			
			if(age != null){
				return moment().diff(age, 'years');
			}else{
				return null;
			};
			
		};
		
		/**
		 * isEndMenorStart
		 *
		 * Passado uma objeto Range, verifica se a hora final é maior que a hora inicial.
		 * EX: Utilizado durante o cadastro de períodos de profissional - Cadastro apenas de horarios - Hora final nao pode ser maior que a hora inicio.
		 * @param  data - data, f FORMAT
		 * @return Boolean
		 */
		function isHoraEndMenorStart(range){

			if( (moment(range.end).isBefore(moment(range.start)) )  ||  (moment(range.end).isSame(moment(range.start)) )   ){
				return true;
			};
			
			return false;
			
		};
		
		/**
		 * isPeriodoSobrepondoOutros
		 *
		 * Verifica se o período ( range ) passado está sobrepondo outros períodos da lista também informada.
		 * EX: Cadastrando os profissionais.
		 * @param  range - Range Object
		 * @param  listaRange - Lista de ranges
		 * @return Boolean
		 */
		function isPeriodoSobrepondoOutros(range,listaRange){

			for(var i=0;i<listaRange.length;i++){

				if(range.idDay == listaRange[i].idDay && range.overlaps(listaRange[i])){
					return true;
				}else{
					continue;
				};
			};
			
			return false;
			
		};

    };
	
})();
