﻿(function () {
    'use strict';

    angular.module('app').factory('ConvenioClienteService', ConvenioClienteService);

    ConvenioClienteService.$inject = ['$http', '$q'];

    function ConvenioClienteService($http, $q) {
        
		var listOpeSau = [];
		var listEmpConv = [];
		var listaOutConv = [];
		
		var service = {
            getListaOperadoraSaude: getListaOperadoraSaude,
			getListaEmpresaConveniada: getListaEmpresaConveniada,
			getListaOutrosConvenios: getListaOutrosConvenios
        };

        return service;

		function getListaOperadoraSaude(){

			if(listOpeSau == null || listOpeSau.length < 1){
				
				//TODO Temporario ate recuperar dados do banco.
				var deferred = $q.defer();
				
				listOpeSau = [{"id":"1","nmOpe":"Bradesco Saúde"},{"id":"2","nmOpe":"Unimed"}];
				
				deferred.resolve(listOpeSau);	
			
				return deferred.promise;
			
			}else{
				
				var deferred = $q.defer();

				deferred.resolve(listOpeSau);	
			
				return deferred.promise;

			};

		};
		
		function getListaEmpresaConveniada(){

			if(listEmpConv == null || listEmpConv.length < 1){
			
				//TODO Temporario ate recuperar dados do banco.
				var deferred = $q.defer();
				
				listEmpConv = [{"id":"1","nmEmp":"Petrobras"},{"id":"2","nmEmp":"Ipiringa"}];
				
				deferred.resolve(listEmpConv);	
			
				return deferred.promise;
			
			}else{
				
				var deferred = $q.defer();

				deferred.resolve(listEmpConv);	
			
				return deferred.promise;

			};

		};
		
		function getListaOutrosConvenios(){

			if(listaOutConv == null || listaOutConv.length < 1){
				
				//TODO Temporario ate recuperar dados do banco.
				var deferred = $q.defer();
				
				listaOutConv = [{"id":"1","nmCon":"Peixe Urbano"},{"id":"2","nmCon":"Nacional"}];
				
				deferred.resolve(listaOutConv);	
			
				return deferred.promise;
			
			}else{
				
				var deferred = $q.defer();

				deferred.resolve(listaOutConv);	
			
				return deferred.promise;

			};

		};

    };
	
})();
