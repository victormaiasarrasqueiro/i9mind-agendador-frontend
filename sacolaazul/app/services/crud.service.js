﻿(function () {
    'use strict';

    angular.module('app').factory('CRUDService', CRUDService);

    CRUDService.$inject = ['$http', '$q'];

    function CRUDService($http, $q) {

		var dominio = 'http://localhost:8081/i9Mind-Agendador/';
		var config = {headers: {'Content-Type': 'application/json'}};
		
		/*
		* OPERAÇÕES BÁSICAS
		*/
		var crudOptions = {
			"inserir": inserir,
			"atualizar": atualizar,
			"remover": remover,
			"listar": listar,
			"obter":obter
        };
		
		/*
		* SERVICES
		* context: Mapeamento no servidor para o serviço.
		* cache: Lista em cache. ( list -> Lista de Objetos | refresh -> Marcado para atualizar a lista | last -> Ultima atualização | timeExp -> Tempo limite para realizar refresh
		*/
		var atual = "";
		var services = {
			especialidade: { "context":"especialidade", "cache":{ "list":[],"refresh":true,"last":moment(),"timeExp":0 } },
			operadorasaude: { "context":"operadorasaude", "cache":{ "list":[],"refresh":true,"last":moment(),"timeExp":0 } },
			profissional: { "context":"profissional", "cache":{ "list":[],"refresh":true,"last":moment(),"timeExp":0 } }
		};
		
		/*
		* SERVICE FACTORY
		*/
		var factory = {
			especialidade: basicFactoryEspecialidade,
			operadorasaude: basicFactoryOperadoraSaude,
			profissional: basicFactoryProfissional
        };
		
        return factory;
		
		/*
		* BASIC FACTORY
		* - Construtor de Serviço com as operações básicas de um CRUD
		*/
		function basicFactoryEspecialidade(){
		
			atual = "especialidade";
			return crudOptions;	
			
		};
		function basicFactoryOperadoraSaude(){
			
			atual = "operadorasaude";
			return crudOptions;	
			
		};
		function basicFactoryProfissional(){
			
			atual = "profissional";
			return crudOptions;	
			
		};
		
		/*
		*
		* obter
		* Obter o objeto com o id informado.
		*
		*/
		function obter(id) {
			
			var url = dominio + services[atual].context + '/obter/' + id;

			var config = {headers: {'Content-Type': 'application/json'}};

			return $http.get(url, config).then(function(response) {
				return response.data.object;
			});
			
        };
		
		/*
		* inserir(model)
		* Insere um registro para a empresa logada.
		*/
		function inserir(model) {
			
			var url = dominio + services[atual].context + '/inserir';
			
			return $http.post(url, angular.toJson( model ), config).then(function(response) {
				setRefresh();
				return response.data;
			});
			
        };
		
		/*
		* atualizar
		* Atualiza uma especialidade para a empresa logada.
		*/
		function atualizar(model) {
			
			var url = dominio + services[atual].context + '/atualizar';

			return $http.put(url, angular.toJson( model ), config).then(function(response) {
				setRefresh();
				return response.data;
			});

        };
		
		/*
		* remover
		* Remove uma especialidade para a empresa logada.
		*/		
		function remover(id) {
			
			var url = dominio + services[atual].context + '/excluir/' + id;

			return $http.delete(url, config).then(function(response) {
				setRefresh();
				return response.data;
			});
			
        };

		/*
		* listar
		* Obter uma lista de os especialidades cadastradas da empresa cadastrada.
		*/
        function listar() {
		
			setTimeRefresh();

			if(services[atual].cache.list == null || services[atual].cache.list.length < 1 || services[atual].cache.refresh ){
				
				var url = dominio + services[atual].context + '/listar';
				
				return $http.get(url, config).then(function(response) {
			
					setListCache(response.data.object);
					return response.data.object;
					
				});
				
			}else{

				var deferred = $q.defer();
				deferred.resolve(services[atual].cache.list);	
				return deferred.promise;
			
			};
			
        };
		
		/*
		* setListCache(list)
		* Inserindo dados na lista de cache.
		*/
		function setListCache(list){
			services[atual].cache.list = list;
			services[atual].cache.last = moment();
			services[atual].cache.refresh = false;
		};
		
		/*
		* setRefresh()
		* Agendar para atualizar a lista quando preciso.
		*/
		function setRefresh(){
			services[atual].cache.refresh = true;
		};
		
		/*
		* setRefresh()
		* Agendar para atualizar a lista de especialidades quando preciso.
		*/
		function setTimeRefresh(){
		
			//if(cache.timeExp != 0){

				//alert(moment().diff(cache.timeExp));

			//};
			
		};

    };
	
})();
