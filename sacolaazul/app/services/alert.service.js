﻿(function () {
    'use strict';

    angular.module('app').factory('AlertService', AlertService);

    AlertService.$inject = ['toastr'];

    function AlertService(toastr) {
        
		var mensagens = []; // Define as mensagens de alerta
		
		var service = {
			sweetRemoverObjeto: sweetRemoverObjeto,
			sweetSucesso:sweetSucesso,
			toastrErro:toastrErro
        };

        return service;

		/**
		 * sweetRemoverObjeto
		 *
		 * Alert solicitando a confirmação da exclusão de um recurso no banco de dados.
		 * @param nomeObj - Nome do Objeto que irá aparecer na mensagem.
		 * @param funcao - Funcão que irá ser chamada apos a confirmação.
		 */
		function sweetRemoverObjeto(nomeObj,funcao) {

			swal
			(
				{
					title: "Você tem certeza?c", 
					text: "Você realmente deseja remover " + nomeObj + " do sistema?", 
					type: "warning",
					cancelButtonText: "Cancelar",
					showCancelButton: true,
					closeOnConfirm: false,
					closeOnCancel: true, 
					confirmButtonText: "Sim, remova " + nomeObj ,
					confirmButtonColor: "#70bafd"
				}, 
				function() 
				{
					return funcao();		
				}
				
			);
			
		};
		
		/**
		 * sucesso
		 *
		 * Alert informando sucesso na operação.
		 * @param nomeObj - Nome do Objeto que irá aparecer na mensagem.
		 * @param funcao - Funcão que irá ser chamada apos a confirmação.
		 */
		function sweetSucesso(idOp, nomeObj, funcao){
			
			var operacao = "";
			var texto = "";
			
			switch (idOp) {

				case 0:

					operacao = "Sucesso!";
					texto = "A operação foi realizada com sucesso!"
					break;

				case 1:

					operacao = "Incluido!";
					texto = nomeObj + "foi incluido no sistema com sucesso!";
					break;
					
				case 2:

					operacao = "Alterado!";
					texto = nomeObj + "foi alterado no sistema com sucesso!";
					break;
					
				case 3:

					operacao = "Removido!";
					texto = nomeObj + "foi removido do sistema com sucesso!";
					break;
			};
			
			swal
			(
				{
					title: operacao, 
					text: texto, 
					type: "success",
					showCancelButton: false,
					closeOnConfirm: true,
					confirmButtonText: "Ok",
					confirmButtonColor: "#70bafd"
				}, 
				function() 
				{
					if(funcao != null){
						funcao();
					};
				}
			);

		};
		
		/**
		 * toastrErro
		 *
		 * Alert toastr exibindo uma mensagem de erro
		 *
		 * @param text - Mensagem de Erro
		 * @param title - Titulo do Erro
		 */
		function toastrErro(title,text) {
			toastr.error(text,title);
		};

    };
	
})();
