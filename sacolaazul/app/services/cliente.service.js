﻿(function () {
    'use strict';

    angular.module('app').factory('ClienteService', ClienteService);

    ClienteService.$inject = ['$http', '$q'];

    function ClienteService($http, $q) {
        
		var dominio = 'http://www.i9mind.com:8080/i9Mind-Agendador/';
		
		var listaCliente = {"data":[],"refresh":true};
		
		
		var service = {
            obterCliente: obterCliente,
			inserirCliente: inserirCliente,
			atualizarCliente: atualizarCliente,
			removerCliente: removerCliente,
			listarCliente: listarCliente,
			recuperarClienteRemovido: recuperarClienteRemovido,
			listarClienteRemovido: listarClienteRemovido
        };

        return service;
		
		/*
		*
		* function: obterCliente
		* Obter os clientes cadastrados da empresa.
		*
		*/
		function obterCliente(id) {

			var url = dominio + 'cliente/obterCliente/' + id;
			var config = {headers: {'Content-Type': 'application/json'}};

			return $http.get(url, config).then(function(data, status, headers, config) {
				return data.data;
			});
			
        };
		
		/*
		*
		* function: inserirCliente
		* Insere um cliente para a empresa logada.
		*
		*/
		function inserirCliente(model) {
			
			var url = dominio + 'cliente/inserirCliente';
			var config = {headers: {'Content-Type': 'application/json'}};

			return $http.post(url, angular.toJson( model ), config).then(function(data, status, headers, config) {
				agendarListaCliente();
				return data;
			});
			
        };
		
		/*
		*
		* function: atualizarCliente
		* Atualiza um cliente para a empresa logada.
		*
		*/
		function atualizarCliente(model,callbackSuccess,callbackError) {
			
			var url = dominio + 'cliente/atualizarCliente';
			var config = {headers: {'Content-Type': 'application/json'}};

			return $http.put(url, angular.toJson( model ), config).then(function(data, status, headers, config) {
				agendarListaCliente();
				return data;
			});

        };
		
		/*
		*
		* function: removerCliente
		* Remove um cliente para a empresa logada.
		*
		*/		
		function removerCliente(id) {
			
			var url = dominio + 'cliente/removerCliente/' + id;
			var config = {headers: {'Content-Type': 'application/json'}};

			return $http.delete(url, config).then(function(data, status, headers, config) {
				agendarListaCliente();
				return data;
			});
			
        };

		/*
		*
		* function: obterCliente
		* Obter os clientes cadastrados da clinica.
		*
		*/
        function listarCliente() {

			if(listaCliente.data == null || listaCliente.data.length < 1 || listaCliente.refresh ){
				
				var url = dominio + 'cliente/listarCliente';
				var config = {headers: {'Content-Type': 'application/json'}};

				return $http.get(url, config).then(function(data, status, headers, config) {
					
					setListaCliente(data);
					
					return data;
					
				});
				
			}else{
				
				var deferred = $q.defer();

				deferred.resolve(listaCliente.data);	
			
				return deferred.promise;
			
			};
			
        };

		/*
		*
		* function: recuperarClienteRemovido
		* Recupera um cliente removido.
		*
		*/
		function recuperarClienteRemovido(model,callbackSuccess,callbackError) {
			
			var url = dominio + 'cliente/recuperarClienteRemovido';
			var config = {headers: {'Content-Type': 'application/json'}};

			$http.put(url, angular.toJson( model ), config).success(callbackSuccess).error(callbackError);

        };
		
		/*
		*
		* function: listarClienteRemovido
		* Obter os clientes removidos da clinica.
		*
		*/
        function listarClienteRemovido(callbackSuccess,callbackError) {

			var url = dominio + 'cliente/listarClienteRemovido';
			var config = {headers: {'Content-Type': 'application/json'}};

			$http.get(url, config).success(callbackSuccess).error(callbackError);
				
        };
		
		// Inserindo dados na lista cliente.
		function setListaCliente(data){
		
			listaCliente.data = data;
			listaCliente.lastUpdate = moment();
			listaCliente.refresh = false;

		};
		
		// Agendar para atualizar a lista de clientes quando preciso.
		function agendarListaCliente(){
			
			listaCliente.refresh = true;
			
		};

    };
	
})();
