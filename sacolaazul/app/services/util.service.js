﻿(function () {
    'use strict';

    angular.module('app').factory('UtilService', UtilService);

    UtilService.$inject = [];

    function UtilService() {
        
		
		var service = {
            getObjectFromList: getObjectFromList,
			removeObjectList: removeObjectList,
			updateObjectFromList: updateObjectFromList,
			contains: contains,
			enumArray: enumArray
        };

        return service;
		
		
		/**
		 * getObjectFromList
		 *
		 * Retorna o primeiro objeto da lista com a propriedade igual ao valor passado.
		 * @param  valor - valor a ser buscado.
		 * @param  prop  - propriedade no qual o valor ser comparado.
		 * @param  lista - lista onde será buscado o objeto.
		 * @return objeto encontrado
		 */
		function getObjectFromList(valor,prop,lista){
			
			for(var i = 0 ; i < lista.length ; i++){
				
				if(lista[i][prop] == valor){
					return angular.copy(lista[i]);
				};
				
			};
			
			return null;
			
		};
		
		/**
		 * updateObjectFromList
		 *
		 * Atualiza o objeto encontrado na lista.
		 * @param  valor - valor a ser buscado.
		 * @param  prop  - propriedade no qual o valor ser comparado.
		 * @param  model  - Objeto que será inserido.
		 * @param  lista - lista onde será buscado o objeto.
		 * @return objeto encontrado
		 */
		function updateObjectFromList(valor,prop,model,lista){
			
			for(var i = 0 ; i < lista.length ; i++){
				
				if(lista[i][prop] == valor){
					lista[i] = angular.copy(model);
					return lista;
				};
				
			};
			
			return null;
			
		};

		/**
		 * removeObjectList
		 *
		 * Remove o objeto com que possui o valor da propriedade da lista passada.
		 * @param  valor - valor a ser buscado.
		 * @param  prop  - propriedade no qual o valor ser comparado.
		 * @param  lista - lista onde será removido o objeto.
		 * @return lista com o objeto removido.
		 */
		function removeObjectList(valor,prop,lista) {
			var lT = angular.copy(lista);
			for(var i = 0; i < lT.length; i++){

				if(lT[i][prop] == valor){
					lT.splice(i,1);
					break;
				};
			};
			
			return lT;
		
		};
		
		/**
		 * contains
		 *
		 * Verifica se na lista existe um objeto com as propriedades informadas.
		 *
		 * @param  valor - valor a ser buscado.
		 * @param  prop  - propriedade no qual o valor ser comparado.
		 * @param  lista - lista onde será removido o objeto.
		 * @return lista com o objeto removido.
		 */
		function contains(valor,prop,lista) {
			var lT = angular.copy(lista);
			for(var i = 0; i < lT.length; i++){

				if(lT[i][prop] == valor){
					return true;
				};
			};
			
			return false;
		
		};
		
		/*
		* enumerarArray()
		*
		* Enumera um array passado.
		*/
		function enumArray(array){
			var lt = angular.copy(array);
			for(var i=0;i<lt.length;i++){
				lt[i].id = i;
			};
			
			return lt;
		};
		
    };
	
})();
