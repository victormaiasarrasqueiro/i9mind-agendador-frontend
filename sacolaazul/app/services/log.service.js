﻿(function () {
    'use strict';

    angular.module('app').factory('LogService', LogService);

    LogService.$inject = ['$log'];

    function LogService($log) {
									  
		var service = {
			error:error
        };

        return service;

		/**
		 * error
		 *
		 * Registra um evento de erro.
		 *
		 * @param  e - Objeto Erro.
		 */
		function error(e) {
			alert("Erro:" + e.toString());
			$log.info("O erro abaixo já foi enviado a nossa equipe. Caso for requisitado, favor copiar o texto abaixo e enviar a nossa equipe.");
			$log.error(e);
			$log.info("Obrigado e desculpe o transtorno!");
		};

    };
	
})();
