﻿(function () {
    'use strict';

    angular.module('app').factory('TabelaService', TabelaService);

    TabelaService.$inject = ['$http', '$q'];

    function TabelaService($http, $q) {
        
		var ltTpECi = [];    				// listaTipoEstadoCivil - Lista de Tipos de Estados Civis
		var ltOCel  = [];                   // listaOperadoraCelular - Lista de Operadoras de Celular.
		var ltTpSex = [];					// listaTipoSexo - Lista de Tipos de Sexo
		
		var service = {
			getListaTpECivil:getListaTpECivil,
			getListaOCel:getListaOCel,
			getListaTpSex:getListaTpSex
        };

        return service;
		
		/*
		* getListaTpECivil()
		*
		* Retorna uma lista com tipos de estado civís.
		*/
		function getListaTpECivil(){
			
			var deferred = $q.defer();
			
			if(ltTpECi == null || ltTpECi.length < 1){
				ltTpECi = [{"id":0,"ds":"Não Informado"},{"id":1,"ds":"Solteiro"},{"id":2,"ds":"Casado(a)"},{"id":3,"ds":"Divorciado(a)"}];
			};

			deferred.resolve(ltTpECi);	
			
			return deferred.promise;
			
		};
		
		/*
		* getListaOCel()
		*
		* Retorna uma lista de operadoras de celulares.
		*/
		function getListaOCel(){
			
			var deferred = $q.defer();
			
			if(ltOCel == null || ltOCel.length < 1){
				ltOCel = [{"id":0,"ds":"Não Informado"},{"id":1,"ds":"Vivo"},{"id":2,"ds":"TIM"},{"id":3,"ds":"Claro"}];
			};

			deferred.resolve(ltOCel);	
			
			return deferred.promise;
			
		};
		
		/*
		* getListaTpSex()
		*
		* Retorna uma lista de Sexos
		*/
		function getListaTpSex(){
			
			var deferred = $q.defer();
			
			if(ltTpSex == null || ltTpSex.length < 1){
				ltTpSex = [{"id":0,"ds":"Não Informado"},{"id":1,"ds":"Masculino"},{"id":2,"ds":"Feminino"}];
			};

			deferred.resolve(ltTpSex);	
			
			return deferred.promise;
			
		};

    };
	
})();
