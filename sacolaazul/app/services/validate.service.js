﻿(function () {
    'use strict';

    angular.module('app').factory('ValidateService', ValidateService);

    ValidateService.$inject = [];

    function ValidateService() {
        
		var service = {
            hasDuplicidade: hasDuplicidade,
			isStrEmpty:isStrEmpty,
			isListEmpty:isListEmpty
        };

        return service;
		
		/**
		 * hasDuplicidade
		 * Retorna true se existe um objeto igual na lista.
		 */
		function hasDuplicidade(valor,prop,lista){
			
			if(lista != null && lista.length > 0){
				for(var i = 0 ; i < lista.length ; i++){
					
					if(lista[i][prop] == valor){
						return true;
					};
					
				};
			};
			
			return false;
			
		};
		
		/**
		 * isStrEmpty
		 * Verifica se o valor passado é vazio.
		 */
		function isStrEmpty(valor){
			return ( valor == null || angular.isUndefined(valor) || valor.toString().trim() == "" );
			
		};
		
		/**
		 * isListEmpty
		 * Verifica se o valor passado é vazio.
		 */
		function isListEmpty(valor){
			return ( valor == null || angular.isUndefined(valor) || valor.length == 0 );
		};
		
    };
	
})();
