﻿(function () {
    'use strict';

    angular.module('app').factory('CRUDService', CRUDService);

    CRUDService.$inject = ['$http', '$q'];

    function CRUDService($http, $q) {

		var dominio = 'http://www.i9mind.com:8080/i9Mind-Agendador/';
		var config = {headers: {'Content-Type': 'application/json'}};
		
		/*
		* OPERAÇÕES BÁSICAS
		*/
		var crudOptions = {
			"inserir": inserir,
			"atualizar": atualizar,
			"remover": remover,
			"listar": listar
        };
		
		/*
		* SERVICES
		* context: Mapeamento no servidor para o serviço.
		* cache: Lista em cache. ( list -> Lista de Objetos | refresh -> Marcado para atualizar a lista | last -> Ultima atualização | timeExp -> Tempo limite para realizar refresh
		*/
		var atual = "";
		var services = {
			especialidade: { "context":"especialidade", "cache":{ "list":[],"refresh":true,"last":moment(),"timeExp":0 } }
		};
		
		/*
		* SERVICE FACTORY
		*/
		var factory = {
			especialidade: basicFactory("especialidade")
        };
		
        return factory;
		
		/*
		* BASIC FACTORY
		* - Construtor de Serviço com as operações básicas de um CRUD
		*/
		function basicFactory(service){
			atual = service;
			return crudOptions;		
		};
		
		/*
		* inserir(model)
		* Insere um registro para a empresa logada.
		*/
		function inserir(model) {
			
			var url = dominio + services[atual].context + '/inserirEspecialidade';
			
			return $http.post(url, angular.toJson( model ), config).then(function(response) {
				setRefresh();
				return response.data;
			});
			
        };
		
		/*
		* atualizar
		* Atualiza uma especialidade para a empresa logada.
		*/
		function atualizar(model) {
			
			var url = dominio + services[atual].context + '/atualizarEspecialidade';

			return $http.put(url, angular.toJson( model ), config).then(function(response) {
				setRefresh();
				return response.data;
			});

        };
		
		/*
		* remover
		* Remove uma especialidade para a empresa logada.
		*/		
		function remover(id) {
			
			var url = dominio + services[atual].context + '/removerEspecialidade/' + id;

			return $http.delete(url, config).then(function(response) {
				setRefresh();
				return response.data;
			});
			
        };

		/*
		* listar
		* Obter uma lista de os especialidades cadastradas da empresa cadastrada.
		*/
        function listar() {
			
			setTimeRefresh();
			
			if(services[atual].cache.list == null || services[atual].cache.list.length < 1 || services[atual].cache.refresh ){
				
				alert("Indo ao banco");
				
				var url = dominio + services[atual].context + '/listarEspecialidade';

				return $http.get(url, config).then(function(response) {
					setListCache(response.data);
					return response.data;
				});
				
			}else{
				
				alert("Pegando do cache");
				
				var deferred = $q.defer();

				deferred.resolve(services[atual].cache.list);	
			
				return deferred.promise;
			
			};
			
        };
		
		/*
		* setListCache(list)
		* Inserindo dados na lista de cache.
		*/
		function setListCache(list){
			services[atual].cache.list = list;
			services[atual].cache.last = moment();
			services[atual].cache.refresh = false;
		};
		
		/*
		* setRefresh()
		* Agendar para atualizar a lista de especialidades quando preciso.
		*/
		function setRefresh(){
			services[atual].cache.refresh = true;
		};
		
		/*
		* setRefresh()
		* Agendar para atualizar a lista de especialidades quando preciso.
		*/
		function setTimeRefresh(){
			
			//if(cache.timeExp != 0){

				//alert(moment().diff(cache.timeExp));

			//};
			
		};

    };
	
})();
