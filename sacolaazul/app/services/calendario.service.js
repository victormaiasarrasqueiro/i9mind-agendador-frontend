﻿(function () {
    'use strict';

    angular.module('app').factory('CalendarioService', CalendarioService);

    CalendarioService.$inject = ['$http', '$q'];

    function CalendarioService($http, $q) {

		var empresaCalendario = null;
		
		var defaultCalendar = 
		{
			defaultView:'agendaWeek', 
			header:{
			  left: 'title',
			  center: '',
			  right: 'agendaDay,agendaWeek prev,today,next'
			},
			buttonText:{
				week:     'Agenda Semanal',
				day:      'Agenda de Hoje',
				month: 'Agenda Mensal'
			},
			dayNames:['Dom', 'Seg', 'Ter', 'Qua','Qui', 'Sex', 'Sab'],
			lang: 'pt-br',
			height: "auto",
			timeFormat: 'HH:mm',
			axisFormat: 'HH:mm',
			editable: true,   
			weekends:false,             		  // Trabalha fins de semana. (A ser configurado pelo usuario)
			allDaySlot:false,
			titleFormat:"DD MMMM",      		  // Formato de Data do Titulo
			columnFormat:"dddd / DD",   		  // Formato de Hora do na Coluna
			minTime:"08:00:00",        			  // Hora que ira comecar o calendario (Uma hora antes do inicio do expediente)
			maxTime:"19:00:00",         		  // Hora que ira terminar o calendario. (Duas horas depois do inicio do expediente)
			selectable: true,           		  // Permite selecionar um range de horas/datas
			selectHelper: true,
			unselectAuto:true,
			droppable:true,
			timezone:"local",
			defaultTimedEventDuration:"00:15:00", // Default para eventos que nao possuem o periodo de termino setado.
			forceEventDuration:true,    		  // Ira formar uma data/hora final para eventos sem este atributo. (Ira usar o atributo defaultTimedEventDuration/defaultAllDayEventDuration para calculo )
			slotDuration:"00:15:00",    		  // Intervalos que serao possivel agendar um evento. (A ser configurado pelo usuario)
			snapDuration:"00:15:00",    		  // Duracao de um evento. (A ser configurado pelo usuario)
			businessHours:{             		  // Horario do expediente. (A ser configurado pelo usuario)
				start: '09:00',         	      // Hora de incio (A ser configurado pelo usuario)
				end: '18:00',           		  // Fim de expediente (A ser configurado pelo usuario)
				dow: [ 1, 2, 3, 4, 5 ]  		  // Dias da semana do expediente. (A ser configurado pelo usuario)
			}
		};
		  
		var service = {
			carregarConfiguracaoCalendarioEmpresa: carregarConfiguracaoCalendarioEmpresa
        };

        return service;
		
		/**
		 * carregarConfiguracaoCalendarioEmpresa()
		 *
		 * Retorna um objeto do servidor com propriedades para a montagem do calendario personalizado do usuário.
		 * @return objeto com as propriedades
		*/
        function carregarConfiguracaoCalendarioEmpresa() {
			
			var deferred = $q.defer();
			var confReturn = {};
			
			if(empresaCalendario == null){
				
				empresaCalendario = angular.copy(defaultCalendar);
				
				return $http({method: 'GET', url: 'config-calendar-user.json'}).then(
					
					function(data, status, headers, config) {

						confReturn = data.data;
						
						empresaCalendario = montarCalendarioRetorno(empresaCalendario,confReturn);

						deferred.resolve(empresaCalendario);	
						return deferred.promise;

					},
					function(data, status, headers, config) {
						
						//Zerando as configurações para tentar recuperar novamente as informações em nova requisição.
						empresaCalendario = null;
						
						deferred.resolve(defaultCalendar);	
						return deferred.promise;
						
					}

				);
				
			}
			else
			{
				deferred.resolve(empresaCalendario);	
				return deferred.promise;

			};

        };
		
		function montarCalendarioRetorno(inicial,personalizado){
		
			inicial.defaultView 	= personalizado.defaultView;
			inicial.weekends 		= personalizado.weekends;
			inicial.minTime 		= personalizado.minTime;
			inicial.maxTime 		= personalizado.maxTime;
			inicial.defaultTimedEventDuration = personalizado.defaultTimedEventDuration;
			inicial.slotDuration 	= personalizado.slotDuration;
			inicial.snapDuration 	= personalizado.snapDuration;
			inicial.businessHours = personalizado.businessHours;
		
			return inicial;
		};
		

		
    };
	
})();