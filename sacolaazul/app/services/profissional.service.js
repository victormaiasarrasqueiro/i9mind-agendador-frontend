﻿(function () {
    'use strict';

    angular.module('app').factory('ProfissionalService', ProfissionalService);

    ProfissionalService.$inject = ['$http', '$q'];

    function ProfissionalService($http, $q) {
        
		var dominio = 'http://www.i9mind.com:8080/i9Mind-Agendador/';
		
		var listaProf = {"data":[],"refresh":true};
		
		var service = {
			"inserir": inserir,
			"atualizar": atualizar,
			"remover": remover,
			"listar": listar
        };

        return service;

		/*
		* inserir(model)
		* Insere um registro para a empresa logada.
		*/
		function inserir(model) {
			
			var url = dominio + 'profissional/inserir';
			var config = {headers: {'Content-Type': 'application/json'}};
			
			return $http.post(url, angular.toJson( model ), config).then(function(response) {
				setRefresh();
				return response.data;
			});
			
        };
		
		/*
		*
		* function: atualizarCliente
		* Atualiza um cliente para a empresa logada.
		*
		*/
		function atualizar(model,callbackSuccess,callbackError) {
			
			var url = dominio + 'profissional/atualizar';
			var config = {headers: {'Content-Type': 'application/json'}};

			return $http.put(url, angular.toJson( model ), config).then(function(data, status, headers, config) {
				agendarListaCliente();
				return data;
			});

        };
		
		/*
		*
		* function: removerCliente
		* Remove um cliente para a empresa logada.
		*
		*/		
		function remover(id) {
			
			var url = dominio + 'profissional/excluir/' + id;
			var config = {headers: {'Content-Type': 'application/json'}};

			return $http.delete(url, config).then(function(data, status, headers, config) {
				agendarListaCliente();
				return data;
			});
			
        };

		/*
		*
		* function: obterCliente
		* Obter os clientes cadastrados da clinica.
		*
		*/
        function listar() {

			if(listaCliente.data == null || listaCliente.data.length < 1 || listaCliente.refresh ){
				
				var url = dominio + 'profissional/listarCliente';
				var config = {headers: {'Content-Type': 'application/json'}};

				return $http.get(url, config).then(function(data, status, headers, config) {
					
					setListaCliente(data);
					
					return data;
					
				});
				
			}else{
				
				var deferred = $q.defer();

				deferred.resolve(listaCliente.data);	
			
				return deferred.promise;
			
			};
			
        };

    };
	
})();
