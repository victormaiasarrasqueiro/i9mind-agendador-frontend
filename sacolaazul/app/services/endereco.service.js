﻿(function () {
    'use strict';

    angular.module('app').factory('EnderecoService', EnderecoService);

    EnderecoService.$inject = ['$http', '$q'];

    function EnderecoService($http, $q) {
        
		var listaUF = [];
		
		var service = {
            getEnderecoCep: getEnderecoCep,
			getListaUF:getListaUF
        };

        return service;

        function getEnderecoCep(cepArg) {
			
			var urlServico = 'http://cep.correiocontrol.com.br/{CEP}.json';
			var cepPesquisa = cepArg.replace('-', '');

			var urlPesquisa = urlServico.replace('{CEP}',cepPesquisa);
			
			return $http({method: 'GET', url: urlPesquisa}).then(function(data, status, headers, config) {
					return data;
			});

        };
		
		function getListaUF(){
			
			var deferred = $q.defer();
			
			if(listaUF == null || listaUF.length < 1){
				listaUF = [{"id":"0","ds":"Não Informado"},{"id":"AC","ds":"AC"},{"id":"AL","ds":"AL"},{"id":"AM","ds":"AM"},{"id":"AP","ds":"AP"},{"id":"BA","ds":"BA"},{"id":"CE","ds":"CE"},{"id":"DF","ds":"DF"},{"id":"ES","ds":"ES"},{"id":"GO","ds":"GO"},{"id":"MA","ds":"MA"},{"id":"MG","ds":"MG"},{"id":"MS","ds":"MS"},{"id":"MT","ds":"MT"},{"id":"PA","ds":"PA"},{"id":"PB","ds":"PB"},{"id":"PE","ds":"PE"},{"id":"PI","ds":"PI"},{"id":"PR","ds":"PR"},{"id":"RJ","ds":"RJ"},{"id":"RN","ds":"RN"},{"id":"RS","ds":"RS"},{"id":"RO","ds":"RO"},{"id":"RR","ds":"RR"},{"id":"SC","ds":"SC"},{"id":"SE","ds":"SE"},{"id":"SP","ds":"SP"},{"id":"TO","ds":"TO"}];
			};

			deferred.resolve(listaUF);	
			
			return deferred.promise;
			
		};

    };
	
})();
