﻿(function () {
    'use strict';

    angular.module('app').factory('PacienteService', PacienteService);

    PacienteService.$inject = ['$http', '$q'];

    function PacienteService($http, $q) {
        
		var service = {
            getListaPaciente: getListaPaciente,
			deletePaciente: deletePaciente
        };

        return service;

        function getListaPaciente() {

			return $http({method: 'GET', url: 'data1.json'}).
                success(function(data, status, headers, config) {
					return data;
                }).
                error(function(data, status, headers, config) {

                });
				
        };
		
		function deletePaciente() {
			
			var deferred = $q.defer();

			$http({method: 'GET', url: 'data1.json'}).
				success(function(data, status, headers, config) {
					deferred.resolve(data);
				}).
				error(function(data, status, headers, config) {
					deferred.reject(status);
				});
			
			 return deferred.promise;
			
        };
		
    };
	
})();
