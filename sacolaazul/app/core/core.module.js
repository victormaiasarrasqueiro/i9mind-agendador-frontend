(function() {
    'use strict';

    angular.module('app.core', [
        'ui.router',
		'blocks.router',
		'angularMoment',
		'jcs-autoValidate',
		'ui.bootstrap',
		'ui.calendar',
		'angular-img-cropper',
		'ui.bootstrap.datetimepicker',
		'datatables',
		'datatables.bootstrap',
		'datatables.colreorder',
		'vsGoogleAutocomplete',
		'ui.mask',
		'toastr',
		'ngAnimate',
		'datatables.tabletools',
		'angucomplete-alt',
		'app.directives',
		'angular-loading-bar',
		'ngAutodisable',
		'modal.foto',
		'modal.agenda.novoevento'
    ]);

})();
